% Chapter 2

\chapter{Experimental Set up and Data processing} % Main chapter title

\label{Chapter3} % For referencing the chapter elsewhere, use \ref{Chapter1} 

\lhead{Chapter 3. Experimental Set up and Data processing} % This is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------


This chapter will provide a brief overview of the camera hardware and software development kits(SDK's) used. A discussion on the procedure to acquire, and process the images into point clouds is also provided.

\section{Camera Hardware and Set up}
The camera hardware used is a Point Grey Research(PGR) Bumblebee XB3 multi-baseline stereo camera. Since the images of interest are placed closer to the camera system, the usage of the narrow baseline left and center lenses is appropriate. 
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{bbxb3thesis.pdf}
\caption{Bumblebee Stereo Camera with their Camera Coordinate System overlaid}
\label{fig:stereo_geometry_3}
\end{figure}
The images are acquired using PGR's Flycapture SDK with a resolution of 1280 $\times$ 960 pixels, so that the high resolution images will result in a dense and more detailed point cloud. Though, the camera is factory calibrated with the calibration file, and the parameters available to the end user, there is no write access to this file and the software provided does not support user calibration. Therefore, the camera has been calibrated using MATLAB and openCV based on chapter \ref{Chapter2}. The calibration parameters are stored in .mat and .YML file formats for MATLAB and openCV respectively. There has been no special mounting structures used to hold the experimental set up. The stereo camera is mounted on a tripod and pointed towards the object of interest which is placed on a table. The only requirement is that of a chessboard pattern or a known geometric texture beside or beneath the object of interest to render the 3D reconstructed point cloud in the chessboard's or the known pattern's frame of reference.
\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{tablesetup.pdf}
\caption{The Bench Top Setup consisting of the Stereo Camera and the Object of Interest}
\label{fig:benchtop}
\end{figure}
\section{Image Acquisition}
As mentioned in the previous section, the images acquired using FlyCapture SDK are of 1280 $\times$ 960 resolution, though there are options to acquire images in lower resolutions. The camera's exposure level, brightness, and gain are adjusted to acquire sharp images without any blur which otherwise would severely affect the quality of the disparity map. Before starting any of the above steps, the stereo camera has to be initialized using the API \textbf{StereoCameraMode} either in the narrow baseline,\textbf{TWO\textunderscore CAMERA\textunderscore NARROW}, or wide baseline mode,\textbf{TWO\textunderscore CAMERA\textunderscore WIDE}. After initializing the stereo mode, the live feed is started using \textbf{StartCapture()} and a frame is grabbed for processing using \textbf{RetrieveBuffer()}. Then, a few API's pertaining to the processing of the image buffer is used before they are saved as raw images with specified file extensions. In this work, the raw images are saved with \textbf{.pgm} extension. For more details on the API's used from FlyCapture SDK, refer to the manual from Point Grey Research which provides a comprehensive discussion on all the API's in \cite{flycapture}.

\section{Camera Calibration using MATLAB / openCV}
The number of planar chessboard images required to calibrate a camera is typically between 10-15, as mentioned in the chapter \ref{Chapter2}, with the requirement of rich set of views i.e., chessboard's movement over the entire frame. Once the images are available, they can be calibrated either using the Jean-Yves Bouguet's MATLAB toolbox or openCV's routine which are based on Zhang's method described in chapter  \ref{Chapter2}, though there are other calibration SDKs based on different methods. The two important sets of inputs are the physical and pixel coordinates of the chessboard corners. While the physical coordinates are in $\mathbb{R}^3$ with the assumption that the chessboard is in $X-Y$ plane (ie., $Z=0$), the pixel coordinates are in $\mathbb{R}^2$. The accuracy of the pixel coordinates depend on the image quality and the corner detector employed whereas the accuracy of the physical coordinates depend on the quality of the printed chessboard and the measurements made on the cell sizes. A compromise on the quality of any of the two coordinates' measurement will deteriorate the reconstruction process.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{calib.pdf}
\caption{Bouguet's Calibration GUI Window}
\label{fig:calib}
\end{figure}
The first step is to acquire the chessboard corners in a rectangular grid selected by clicking the four extreme corners of the grid within the chessboard pattern as shown in the figure below. 
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{corners.pdf}
\caption{Grid Selection and Corner Detection}
\label{fig:corners}
\end{figure}
Similarly, the grid corners of the entire set of calibration images available are acquired. While acquiring the grid corners, it may be noticed that not all the grid corners match with the image corners owing to distortion. An initial guess of the distortion factor can be used to correct this. For more details on the complete working of the toolbox, refer to the documentation by Bouget in \cite{calibdoc}. The above procedure to calibrate the camera is implemented, and the intrinsic calibration parameters obtained for the Bumblebee stereo camera's left and right lenses are given in the tables below.
\begin{table}[h!]
\centering
\caption{Intrinsic Camera Parameters of Left Camera}
\begin{tabular}[c]{|c|c|c|}
\hline
\textbf{Parameters}&\textbf{Values}&\textbf{Uncertainty}\\
\hline
Focal Length & $f_x^l$:1639.1088 & $\sigma_{f_{x}^l}$:3.1219\\
\cline{2-3}
&$f_y^l$:1637.1330 & $\sigma_{f_{y}^l}$:2.9448\\
\hline
Principal Point & $c_x^l$:700.9075 & $\sigma_{c_{x}^l}$:4.8723\\
\cline{2-3}
&$c_y^l$:462.9105 & $\sigma_{c_{y}^l}$:3.7903\\
\hline
Distortion Coefficient & $k_c^l=\begin{bmatrix}
-0.456417328802608\\ 0.327750832756193 \\ 0.002152924785587\\ -0.002767839124623\\ 0\end{bmatrix}$ & $\sigma_{k_{c}^l}=\begin{bmatrix}
0.009981325878528 \\ 0.079467852827360 \\ 0.000603685435370 \\ 0.000448479666017\\ 0
\end{bmatrix}$\\
\hline
\end{tabular}
\end{table}\label{tab:4.1}

\begin{table}[h!]
\centering
\caption{Intrinsic Camera Parameters of Right Camera}
\begin{tabular}[c]{|c|c|c|}
\hline
\textbf{Parameters}&\textbf{Values}&\textbf{Uncertainty}\\
\hline
Focal Length & $f_x^r$:1650.4200 & $\sigma_{f_{x}^r}$:3.6787\\
\cline{2-3}
&$f_y^r$:1643.6674 & $\sigma_{f_{y}^r}$:3.4363\\
\hline
Principal Point & $c_x^r$:713.8422 & $\sigma_{c_{x}^r}$:5.8291\\
\cline{2-3}
&$c_y^r$:495.6312 & $\sigma_{c_{y}^r}$:4.5570\\
\hline
Distortion Coefficient & $k_c^r=\begin{bmatrix}
-0.426708705899981\\ 0.213321176097743 \\ 0.001505046208274\\ 0.000182595342802\\ 0\end{bmatrix}$ & $\sigma_{k_{c}^r}=\begin{bmatrix}
0.009367749860882 \\ 0.024224229982656 \\ 0.000555165666629 \\ 0.001085820887853 \\ 0
\end{bmatrix}$\\
\hline
\end{tabular}
\end{table}\label{tab:4.2}
While the intrinsic camera parameters are the same for all the calibration images, the extrinsic parameters are distinct across the images as the transformation between the camera coordinates and the chessboard coordinates is unique. For example, the extrinsic parameters for the calibration pattern in Figure \ref{fig:corners} is given in table \ref{tab:4.3} and the corresponding representation of the grid with respect to the camera in 3-D view is shown in figure \ref{fig:extrinsic}.

\begin{table}
\centering
\caption{Extrinsic Parameters of Calibration Pattern in Figure \ref{fig:corners}}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Parameters}&\textbf{Values}&\textbf{Uncertainty}\\
\hline
Rotation Vector & $\mathbf{r}_{cb}^l=\begin{bmatrix}
-1.891480e+00\\-1.798788e+00\\-5.982758e-01\\
\end{bmatrix}$& $\sigma_{\mathbf{r}_{{cb}}^l}=\begin{bmatrix}
1.420118e-03\\2.310418e-03\\3.634326e-03\\
\end{bmatrix}$\\
\hline
Translation Vector & $\mathbf{t}_{cb}^l=\begin{bmatrix}
-6.086051e+01\\-8.987417e+01\\5.108114e+02\\
\end{bmatrix}$ & $\sigma_{\mathbf{t}_{{cb}}^l}=\begin{bmatrix}
1.536529e+00\\1.199599e+00\\1.029456e+00\\
\end{bmatrix}$\\
\hline
\end{tabular}\label{tab:4.3}
\end{table}

\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{extrinsic.pdf}
\caption{Chessboard Pose with respect to Camera}
\label{fig:extrinsic}
\end{figure}

The reprojection error after calibration i.e., error between the image corners and the grid points projected on the image using the camera parameters estimated should be in the sub-pixel range. In the case of the calibration image in Figure \ref{fig:corners}, the reprojected grid points and the corresponding reprojection error is shown in the figure below.

\begin{figure}[h!]
\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=6cm,height=6cm,keepaspectratio]{reproject.pdf}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=6.5cm,height=6.5cm,keepaspectratio]{repropixel.pdf}
\end{subfigure}
\caption{Reprojected Grid Points and The Reprojection Error}
\label{fig:reproject}
\end{figure}
Once the cameras have been calibrated exclusively, the stereo calibration toolbox by Bouget will load the individual calibration files and run the stereo calibration and return the pose of the right camera with respect to the left camera. In the case of Bumblebee multi-baseline stereo, the transformation between the left and center lenses is tabulated in table \ref{tab:4.4}.

\begin{table}
\centering
\caption{Position of the Right Camera with respect to Left Camera}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Parameters}&\textbf{Values}&\textbf{Uncertainty}\\
\hline
Rotation Vector & $r_l^r=\begin{bmatrix}
0.00387\\-0.00617\\0.00470
\end{bmatrix}$ & $\sigma_{r_{l}^r}=\begin{bmatrix}
0.00412\\0.00592\\0.00037
\end{bmatrix}$ \\
\hline
Translation Vector & $t_l^r = \begin{bmatrix}
-119.76107\\-0.21901\\-3.96069
\end{bmatrix}$ & $\sigma_{t_{l}^r}=\begin{bmatrix}
0.23175\\0.13524\\1.15789
\end{bmatrix}$\\
\hline
\end{tabular}\label{tab:4.4}
\end{table}
And the pattern in figure \ref{fig:corners} with respect to both the camera views after the pose estimation is shown in figure \ref{fig:extrinsic_2}.
\begin{figure}[ht!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{extrinsic_2.pdf}
\caption{Chessboard Pose with respect to Left and Right Cameras}
\label{fig:extrinsic_2}
\end{figure}
The calibration procedure is similar in openCV, except there is a function, \textbf{findChessboardCorners()}, to automatically detect the rectangular grid in the image provided the chessboard pattern is asymmetric so that the origin of the grid's frame of reference is uniformly selected to be the top left corner across all the calibration images. If the chessboard pattern is symmetric, the algorithm will still detect the grid, but the origin of the frame of reference will be randomly chosen to be any of the grid's extreme corners which is going to cause inconsistency in the representation of the grid's frame over the entire set of calibration images. For more details on the calibration process using openCV, refer to its documentation in \cite{opencv}.
\section{Point Cloud Generation}
The 3D point cloud can, now, be generated using the disparity images and the camera parameters. Consider, for example, the set up shown in figure \ref{fig:benchtop}. The input grayscale images from the setup are shown below in figure
\begin{figure}[h!]

\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=6.5cm,height=6.5cm,keepaspectratio]{left_pot}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=6.5cm,height=6.5cm,keepaspectratio]{right_pot}
\end{subfigure}
\caption{Input Stereo Pair of Flower Pot}
\label{fig:bench_stereo}
\end{figure}

The corresponding rectified image pair and the disparity image are shown below in figure \ref{fig:rect_pot} and figure \ref{fig:disp_pot} respectively.
\begin{figure}[ht!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{rect_pot.pdf}
\caption{Rectified Images}
\label{fig:rect_pot}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{disp_pot.pdf}
\caption{Disparity Image of the Flower Pot Scene \\ Disparity Search Range: 0-144 pixels, Window Size: 7$\times$7 pixels}
\label{fig:disp_pot}
\end{figure}

From the disparity values and the reprojection matrix, $\mathbf{Q}$, the Z values of the pixels with their disparity values are obtained. The corresponding point cloud is shown in figure \ref{fig:pc_pot}.

\begin{figure}[ht!]
\centering
\includegraphics[width=90mm,height = 90mm,keepaspectratio]{pc_pot.pdf}
\caption{3D Point Cloud of Flower Pot with the RGB Detail}
\label{fig:pc_pot}
\end{figure}

\section{Reconstruction of the pottery shard}

Now, having developed sufficient theory on how to reconstruct a scene from their stereo image, this section will start the discussion on the reconstruction of the pottery shard whose full surface reconstruction is the main goal of this thesis work. A formal introduction to the shard and its origins has been provided in chapter \ref{Chapter4}. The figure \ref{fig:orient234} shows the piece of the pottery(shard) of interest in different orientation with the rim of the shard at the bottom, placed on the table.
\begin{figure}[h!]
\begin{subfigure}[b]{0.3\textwidth}
\centering
\includegraphics[width=4.5cm,height=4.5cm,keepaspectratio]{orient2}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\centering
\includegraphics[width=4cm,height=4.5cm,keepaspectratio]{orient3}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
\centering
\includegraphics[width=4.5cm,height=4.5cm,keepaspectratio]{orient4}
\end{subfigure}
\caption{Pottery Shard in Side Views and Top View}
\label{fig:orient234}
\end{figure}
The surface reconstruction of the full pottery is performed by considering only the point cloud of the shard's edge pixels to have a clear representation of the shard's edge space curve without cluttering it with points from its surface as they would be suffering from inconsistencies in the estimated depth values due to the lack of rich texture. \newline

The shard is positioned in such a way that the edge contour is at an angle other than $0^{\circ}$ or ideally, perpendicular to the SAD window so that the edges will have a strong representation in the SAD cost vector and not suffer from aperture problems,\cite{morgan1997aperture}, which occur if the edges were to be parallel to the SAD window.

Considering the top view of the pottery shard, figure \ref{fig:shard_stereo} shows the stereo image.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{shard_stereo.pdf}
\caption{Stereo Image of the Top View of the Shard}
\label{fig:shard_stereo}
\end{figure}
The corresponding rectified images are shown in figure \ref{fig:shard_rect}.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{shard_rect.pdf}
\caption{Rectified Images of the Top View of the Shard}
\label{fig:shard_rect}
\end{figure}
After rectifying the images, instead of reconstructing the entire image only the edge of the shard is reconstructed. In order to reconstruct the edge, the edge image of the shard from the rectified stereo pair was considered using edge detection, and matching the stereo edge pixels was attempted. 
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{stereo_edge_aug_5.pdf}
\caption{Edge Image Pair of the Shard}
\label{fig:stereo_edge_aug_5}
\end{figure}
But, since the edge contours of the shard in the stereo pair differ, it is difficult to determine the unique correspondences as the sum of absolute differences based matching would return multiple minimums or the disparity would be inconsistent. This is shown in the below figures, where a correspondence is searched for an edge pixel of interest at (867,416) in the left rectified image.
\begin{figure}[h!]
\centering
\includegraphics[width=45mm,height = 45mm,keepaspectratio]{lw_aug_5.png}
\caption{Left Window in the Left Rectified Edge Image}
\label{fig:lw_aug_5}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{rw_aug_5.png}
\caption{Right Windows in the Disparity Search Range of Right Rectified Edge Image}
\label{fig:rw_aug_5}
\end{figure}
And plotting the disparity search range against their corresponding SAD cost values shows that cost values do not appear to be consistent. Ideally, they should have been analogous to a convex curve.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{stamp_cost_aug_5.pdf}
\caption{Disparity Search Range vs SAD Cost Values}
\label{fig:stamp_cost_aug_5}
\end{figure}
Therefore, a slight modification to the above procedure results in respective edge pixels being reconstructed by comparing the texture masked by a window in the left rectified image corresponding to their edge pixels with the texture in the disparity search range of the right rectified image using SAD. The resulting disparity image of the edge pixels of the shard in the left rectified image is shown in figure \ref{fig:shard_disp} and it can be noticed, that a few edge pixels lack disparity values as they have been deemed invalid by the constraints mentioned in chapter \ref{Chapter2}.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{shard_edge.pdf}
\caption{Edge of the Shard from Left Rectified Image}
\label{fig:shard_edge}
\end{figure}
 
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{shard_disp.pdf}
\caption{Disparity Image of the Edge Pixels of the Shard \\ Window Size: 31x31; Disparity Range: 376-434 pixels}
\label{fig:shard_disp}
\end{figure}
The correctness of this procedure is evaluated by considering the same edge pixel of the shard in the left rectified edge image at (867,416) and comparing their texture with the texture in the disparity search range of the right rectified image.

\begin{figure}[h!]
\centering
\includegraphics[width=45mm,height = 45mm,keepaspectratio]{lw_texture_aug_5.pdf}
\caption{Left Window in the Left Rectified Image}
\label{fig:lw_texture_aug_5}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{rw_texture_aug_5.pdf}
\caption{Right Windows in the Disparity Search Range of Right Rectified Image}
\label{fig:rw_texture_aug_5}
\end{figure}
Now, plotting the disparity search range of the right rectified image with their SAD cost values shows that the cost values are convex and fitting a quadratic curve to the cost values gives the sub-pixel correspondence estimate.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{sad_cost_textre_aug_5.pdf}
\caption{Disparity Search Range vs SAD Cost Values using Texture Information}
\label{fig:sad_cost_textre_aug_5}
\end{figure}
The procedure is summarized as a pseudo code in an algorithmic manner in Algorithm \ref{edgealgo}. In lines 2-4, information on the textured stereo pair and the edge pixels of the shard in the left rectified image are assigned to variable names. In line 5, an odd numbered window size is defined. In line 6, a for loop is initiated where the iterator is assigned with the pixel coordinates of the edge pixels of the shard. In line 7, a window of the previously defined size, $window_{size}$, is used to mask the pixel of interest $i$, given by the loop iterator, in the left rectified (textured) image. i.e., the pixel $i$ will be the center element of the window, $Window_{Left}(m/2,m/2)=i$ where $m$ is the window size. In line 8, a nested for loop is initiated with the iterator assigned with the values of the disparity search range in the right rectified image. In line 9, a similar right window is defined which is moved along the $x_{pixel}$ coordinate axis along the disparity search range with every iteration of the loop. In line 10, sum of absolute intensity differences between the two windows is computed which is aggregated using a variable name with every iteration. In line 11, the nested for loop is terminated when traversing the disparity search range is complete. In line 12, the disparity value for the edge pixel $i$ in the left edge image is estimated by finding the minimum of a quadratic curve fitted to the cost values aggregated which are expected to be convex. In line 13, the outer for loop terminates when the disparity values for all the edge pixels have been estimated.

\begin{algorithm}[h!]
\textbf{Function} Edge Disparity Estimation\\
 $I_{L} \in$ Left Rectified Image\\
 $I_R \in $  Right Rectified Image\\
$I_{LE} \in $ Shard Edge Pixels in Left Rectified Edge Image\\
$Window_{size}$ = Odd Integer Value\\

\For{$i \in I_{LE}(x_{pixel},y_{pixel})$}{
 $Window_{Left} = I_L(Window_{size}[i(x_{pixel})])$\;

\For{$d \in Disparity_{Range}$}{
 $Window_{Right} = I_R(Window_{size}[i(x_{pixel})+d])$\;
$ Cost_{SAD}[d] = \vert Window_{Left}-Window_{Right} \vert$ \;
}
 $Disparity[i] = Minimum[Quadratic_{Fit}( Cost_{SAD})]$\;
}
\caption{Edge Disparity Estimation Procedure}
\label{edgealgo}
\end{algorithm}

And, from the disparity values of the edge pixels and the camera matrix $\mathbf{Q}$ the edge pixels are reconstructed and the reconstructed point cloud is shown in figure \ref{fig:shard_recon}.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{shard_recon.pdf}
\caption{Point Cloud of the Edge pixels of the Shard}
\label{fig:shard_recon}
\end{figure}
The point cloud has been rendered in the frame of reference whose origin is at the inner corner of the top left black square in the pattern on which the shard is placed as seen in figure \ref{fig:shard_stereo}. This is implemented by estimating the homography between the four corners from the pattern and its corresponding pixel coordinates provided the physical coordinates of the corners have been measured. Using this edge point cloud, geometric primitives are fitted to estimate the complete shape of the pottery which is the topic of discussion in chapter \ref{Chapter4}.
\section{Summary}
The camera parameters estimated should be reasonable which can be based on the re-projection error. Also, the edge point cloud of the bowl has been estimated using a combination of edge detection and conventional stereo texture matching to obtain fair depth values. This point cloud will serve as the input to the parameter identification techniques discussed in chapter \ref{Chapter4}.