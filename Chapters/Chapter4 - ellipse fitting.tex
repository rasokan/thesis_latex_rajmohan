% Chapter 2

\chapter{Parameter Identification} % Main chapter title

\label{Chapter4} % For referencing the chapter elsewhere, use \ref{Chapter1} 

\lhead{Chapter 3. Parameter Identification} % This is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------

\section{Overview}
This chapter will discuss the techniques used to try and estimate geometric primitives from the point cloud data of rotationally symmetric objects that were generated using the methods described in chapter \ref{chapter3}

\section{Ellipse Fitting}
One of the most predominant geometric primitives used in computer vision applications are ellipses. An ellipse can be algebraically described by the second order polynomial 
\begin{equation}
ax^2+bxy+cy^2+dx+ey+f=0
\end{equation} where $a,b,c,d,e,f$ are the coefficients which characterizes the estimated ellipse and $(x,y)$ are the coordinates of the points to which an ellipse is estimated. In this work, the numerically stable version of the Direct least squares ellipse fitting by Radim is used. It's an improvised version of the Direct Least Squares fitting of ellipses by Fitzgibbon. The constraint for the polynomial to enforce an ellipse specific solution is given by 
\begin{equation}
b^2-4ac < 0
\end{equation}
The polynomial is algebraic distance of the points $(x,y)$ to the conic described by it. Therefore, the objective is to minimize this algebraic distance based on the coefficients. The cost function can be appropriately defined as
\begin{equation}
\mathbf{P}(x,y) = \mathbf{x}.\mathbf{a}=0
\end{equation}
where the coefficients and the polynomial are vectorized as $\mathbf{a} = [a,b,c,d,e,f]^T$ and $\mathbf{x}=[x^2,xy,y^2,x,y,1]$. As usual, the minimization problem is given as finding the coefficients that minimizes the sum of the squared algebraic distances of every coordinate point to the conic.
\begin{equation}
\underset{\mathbf{a}}{\text{minimize}}\sum_{i=1}^{N}{\mathbf{P}(x_i,y_i)}^2
\end{equation}
where $N$ is the number of points available in the cloud. By proper scaling, the inequality constraint can be changed to the equality constraint $4ac^2-b = 1$ since scaling $\mathbf{a}$ does not affect the conic. Therefore the problem is given by
\begin{equation}
E = \underset{\mathbf{a}}{\text{minimize}}\|\mathbf{D} \mathbf{a}\|^2 \hspace{3mm} \text{subject to}\hspace{3 mm} {\mathbf{a}}^T\mathbf{C}\mathbf{a}=1
\end{equation}
where $\mathbf{D} = \begin{bmatrix}
x_i^2&x_iy_i&y_i^2&x_i&y_i&1\\
.&.&.&.&.&.\\
x_N^2&x_Ny_N&y_N^2&x_N&y_N&1
\end{bmatrix}$ is the $N \times 6$ design matrix describing the objective function to be minimized and $\mathbf{C} = \begin{bmatrix}
0&0&2&&&\\0&-1&0&&\mathbf{0}_{3\times 3}&\\2&0&0&&&\\
&\mathbf{0}_{3\times 3}&&&\mathbf{0}_{3\times 3}&
\end{bmatrix}$ is a $6 \times 6$ matrix describing the equality constraint. By using the lagrange multiplier,
\begin{equation}
\begin{aligned}
\mathbf{a}^T\mathbf{D}^T\mathbf{D}\mathbf{a}-\lambda (\mathbf{a}^T\mathbf{C}\mathbf{a}-1)=& 0\\
2\mathbf{D}^T\mathbf{D}\mathbf{a}-2 \lambda \mathbf{C}\mathbf{a}=& 0\\
\mathbf{a}^T\mathbf{C}\mathbf{a}-1 = & 0\\
\end{aligned}
\end{equation}
the problem is simplified to a generalized eigenvalue problem
\begin{equation}
\begin{aligned}
\mathbf{S}\mathbf{a} = & \lambda \mathbf{C}\mathbf{a}\\
\mathbf{a}^T\mathbf{C}\mathbf{a} = &1
\end{aligned}
\end{equation}
where $\mathbf{S}=\mathbf{D}^T\mathbf{D}$ is called the scatter matrix and coefficient vector $\mathbf{a}$ is given by the smallest eigenvalue $\lambda$.
Finding the eigenvalues to the above equation is simplified by decomposing the design matrix $\mathbf{D}$ into quadratic and linear parts.
\begin{equation}
\begin{aligned}
\mathbf{D}_1 =&\begin{bmatrix}
x_i^2&x_iy_i&y_i^2\\.&.&.\\
x_N^2&x_Ny_N&y_N^2
\end{bmatrix}\hspace{5mm}
\mathbf{D}_2 = & \begin{bmatrix}
x_i&y_i&1\\.&.&.\\
x_N&y_N&1
\end{bmatrix}
\end{aligned}
\end{equation}
and the scatter matrix becomes 
\begin{equation}
\mathbf{S} = \begin{bmatrix}
\mathbf{S}_1&\mathbf{S}_2\\
\mathbf{S}_2^T&\mathbf{S}_3
\end{bmatrix}
\end{equation}
where $\mathbf{S}_1 = \mathbf{D}_1^T\mathbf{D}_1$, $\mathbf{S}_2 = \mathbf{D}_1^T\mathbf{D}_2$ and $\mathbf{S}_3 = \mathbf{D}_2^T\mathbf{D}_2$.
And the constraint $\mathbf{C}$ is expressed as
\begin{equation}
\mathbf{C} = \begin{bmatrix}
\mathbf{C}_1&\mathbf{0}\\
\mathbf{0}&\mathbf{0}
\end{bmatrix}
\end{equation}
where $\mathbf{C}_1=\begin{bmatrix}
0&0&2\\0&-1&0\\2&0&0
\end{bmatrix}$.
Also splitting the coefficient vector $\mathbf{a}$ into two 3-dimensional vectors $\mathbf{a}_1=[a,b,c]^T$ and $\mathbf{a}_2=[d,e,f]^T$ such that $\mathbf{a}=\begin{bmatrix}
\mathbf{a}_1\\\mathbf{a}_2
\end{bmatrix}$, the equation 4.7 is defined as
\begin{equation}
\begin{bmatrix}
\mathbf{S}_1&\mathbf{S}_2\\
\mathbf{S}_2^T&\mathbf{S}_3
\end{bmatrix}\begin{bmatrix}
\mathbf{a}_1\\\mathbf{a}_2
\end{bmatrix}=\lambda \begin{bmatrix}
\mathbf{C}_1&\mathbf{0}\\
\mathbf{0}&\mathbf{0}
\end{bmatrix}\begin{bmatrix}
\mathbf{a}_1\\\mathbf{a}_2
\end{bmatrix}
\end{equation}
Here, $\mathbf{S}_3=\mathbf{D}_2^T\mathbf{D}_2$ is a scatter matrix where $\mathbf{D}_2$ is a matrix which defines points on a line. Therefore, the scatter matrix $\mathbf{S}_3$ is singular only if all the points lie on a line which is a case that is clearly not of interest to ellipse fitting. In all other cases the scatter matrix is not singular. The equation 4.11 can be modified as
\begin{equation}
\mathbf{C}_1^{-1}\left(\mathbf{S}_1-\mathbf{S}_2\mathbf{S}_3^{-1}\mathbf{S}_2^T\right)\mathbf{a}_1 = \lambda \mathbf{a}_1
\end{equation} and the equality constraint as
\begin{equation}
\mathbf{a}_1\mathbf{C}_1\mathbf{a}_1 = 1
\end{equation}
Therefore, the generalized eigenvector problem becomes
\begin{equation}
\begin{aligned}
\mathbf{M}\mathbf{a}_1 =& \lambda \mathbf{a}_1\\
\mathbf{a}_1^T\mathbf{C}_1\mathbf{a}_1 =& 1\\
\end{aligned}
\end{equation}where $\mathbf{M}=\mathbf{C}_1^{-1}\left(\mathbf{S}_1-\mathbf{S}_2\mathbf{S}_3^{-1}\mathbf{S}_2^T\right)$
And $\mathbf{a}=\begin{bmatrix}\mathbf{a}_1\\\mathbf{a}_2\end{bmatrix}$ where $\mathbf{a}_2 = -\mathbf{S}_3^{-1}\mathbf{S}_2^T\mathbf{a}_1$ from equation 4.11. Thus finding the eigenvector $\mathbf{a}_1$ of $\mathbf{M}$ is equivalent to finding the eigenvector $\mathbf{a}$ of the minimum positive eigenvalue $\lambda$ in equation 4.7. 

\section{Circle Fitting}
To overcome the difficulties encountered in ellipse fitting due to the shortage of point cloud date, circle fitting has been used since circle was always the intended outcome of the ellipse fitting on a 3D point cloud. First, circle fitting based on Kasa's method was attempted but mostly the output parameters returned was short of the actual values. This disadvantage in using Kasa's method is discussed and the Taubin's circle fitting is explained which returns close to accurate parameter values. 
\subsection{Kasa's method}
As with any conic function, the circle fitting is described as the minimization of the following circle equation,
\begin{equation}
P_{Kasa} = \sum_{i=1}^{N}[(x_i-a)^2+(y_i-b)^2-R^2]^2
\end{equation}
where $N$ is the number of points $(x_i,y_i)$ in the dataset, $a,b,R$ are the estimated center coordinates and radius. A simple change of parameters is used to avoid the non linearity in the derivatives of the original equation. Therefore,
\begin{equation}
P_{Kasa} = \sum_{i=1}^{N}(z_i+Bx_i+Cy_i+D)^2
\end{equation}
where $B=-2a;C=-2b;D=a^2+b^2-R^2$ and also to use minimum number of terms $z_i=x_i^2+y_i^2$.
Differentiating the original equation with respect to $a,b,R$ translates to differentiating the new equation with respect to $B,C,D$.
\begin{equation}
\begin{aligned}
\dfrac{\partial P_{kasa}}{\partial B} =& 2\sum_{i=1}^{N}[z_i+Bx_i+Cy_i+D]x_i=0\\
=&\sum x_i^2B+\sum x_iy_iC+\sum x_iD = -\sum x_i Z\\
\dfrac{\partial P_{kasa}}{\partial C} =&\sum x_iy_iB+\sum y_i^2C+\sum y_iD=-\sum y_iZ\\
\dfrac{\partial P_{kasa}}{\partial D} = &\sum x_iB+\sum y_iC+ND = -\sum z_i\\
\end{aligned}
\end{equation}
The coefficients of the above system of equations can be defined in matrix form as $\mathbf{P}_{kasa}^T \mathbf{P}_{kasa}$ where $\mathbf{P}_{kasa} = \begin{bmatrix}
x_1&y_1&1\\.&.&.\\x_N&y_N&1
\end{bmatrix}$.
Formulating the equation 4.17 as \begin{equation}\mathbf{P}_{kasa}^T\mathbf{P}_{kasa}\mathbf{u} = -\mathbf{P}_{kasa}^T \mathbf{z}
\end{equation} where $\mathbf{u}=[B,C,D]^T$ and $\mathbf{z}=[z_i,.,.,z_N]^T$ the parameter vector $\mathbf{u}$ is derived as
\begin{equation}
\mathbf{u} = -\mathbf{P}_{kasa}^{-}\mathbf{z}
\end{equation}
where $\mathbf{P}_{kasa}^{-}$ is the pseudo-inverse.
\subsection{Taubin's Method}
The Kasa's minimization function can be modified as 
\begin{equation}
P_{kasa} = \sum_{i=1}^{N}d_i^2(d_i+2R)^2
\end{equation}
where $d_i=r_i-R$ is distance between the point $(x_i,y_i)$ and its closest point on the circle.
Since $|d_i|<<R$,
\begin{equation}
P_{kasa} = 4R^2\sum_{i=1}^{N}d_i^2
\end{equation}
Also,
\begin{equation}
R^2\approx \dfrac{1}{N}\sum_{i=1}^{N}(x_i-a)^2+(y_i-b)^2
\end{equation}
Therefore, equation 4.20 can be expressed as
\begin{equation}
P_{kasa}\approx \dfrac{4}{N}\left[\sum_{i=1}^{N}(x_i-a)^2+(y_i-b)^2\right]\left[\sum_{i=1}^{N}d_i^2\right]
\end{equation}
Since the objective here is to minimize $$\sum_{i=1}^{N}d_i^2$$ it can accomplished by minimizing the following
\begin{equation}
P_{taubin}=\dfrac{\sum [(x_i-a)^2+(y_i-b)^2-R^2]}{4N^{-1}\sum [(x_i-a)^2+(y_i-b)^2]}
\end{equation}
Again, by change of parameters $a = -\dfrac{B}{2A}\hspace{2mm}b=-\dfrac{C}{2A}\hspace{2mm}R^2=\dfrac{B^2+C^2-4AD}{4A^2}$,
\begin{equation}
P_{taubin} = \dfrac{\sum [Az_i+Bx_i+Cy_i+D]^2}{N^{-1}\sum [4A^2z_i+4ABx_i+4ACy_i+B^2+C^2]}
\end{equation}
Though the above minimization function was proposed by Taubin, the derivations in this work leading to this minimization function is from the work of Chernov.

The equation 4.25 is equivalent to minimizing the function, \begin{equation}\sum_{i=1}^{N}[Az_i+Bx_i+Cy_i+D]^2
\end{equation}, with the constraint, $4A^2\bar{z}+4AB\bar{x}+4AC\bar{y}+B^2+C^2=1$ where $\dfrac{1}{N}\sum z_i=\bar{z};\dfrac{1}{N}\sum x_i = \bar{x};\dfrac{1}{N}\sum y_i = \bar{y}$ are the first moments. Now to implement the Taubin's method as a Newton-Method few more steps are considered. From the minimization function, it is seen,
\begin{equation}
D=-A\bar{z}-B\bar{x}-C\bar{y}
\end{equation}
Therefore, the equation 4.25 can be rewritten as
\begin{equation}
P_{taubin}=\dfrac{\sum [Az_i+Bx_i+Cy_i+D]^2}{B^2+C^2-4AD}
\end{equation}
Now, the objective function expressed in equation 4.26 is subjected to two constraints, the first given by equation 4.27 and the second constraint, \begin{equation}
B^2+C^2-4AD=1
\end{equation}
If the data is normalized and centred so that the centroids are $\bar{x}=0,\bar{y}=0$ and thus, $D=-A\bar{z}$. And the minimization function, once again, is modified to
\begin{equation}
P_{taubin} = \sum_{i=1}^{N}[A(z_i-\bar{z})+Bx_i+Cy_i]^2
\end{equation} with the constraint also being modified to
\begin{equation}
4\bar{z}A^2+B^2+C^2=1
\end{equation}
In Chernov's work, another parameter $A_0=2\bar{z}^{1/2}A$ is introduced to make the further steps simpler. The equations 4.30 and 4.31 are then modified as,
\begin{equation}
P_{taubin}=\sum_{i=1}^{N}\left[A_0\dfrac{z_i-\bar{z}}{2\bar{z}^{1/2}}+Bx_i+Cy_i\right]^2
\end{equation}
and constraint as
\begin{equation}
A_0^2+B^2+C^2=1
\end{equation}
Again, representing the minimization function in terms of matrix multiplication as in Kasa's method,
\begin{equation}
P_{taubin} = \|\mathbf{X}\mathbf{u}\|^2
\end{equation}
where $\mathbf{X}=\begin{bmatrix}
(z_1-\bar{z})/(2\bar{z}^{1/2})&x_1&y_1&1\\.&.&.&.\\(z_N-\bar{z})/(2\bar{z}^{1/2})&x_N&y_N&1\\
\end{bmatrix}$ and $\mathbf{u}=[A_0,B,C,D]^T$ with the constraint \begin{equation}
\|\mathbf{u}\|=1
\end{equation}
The above minimization can be implemented either using Singular Value Decomposition (SVD) or in a Newton based method.
