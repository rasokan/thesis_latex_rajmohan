% Chapter 2

\chapter{Camera Calibration Techniques} % Main chapter title

\label{Chapter2} % For referencing the chapter elsewhere, use \ref{Chapter1} 

\lhead{Chapter 2. Camera Calibration Techniques} % This is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------

The camera calibration is the first step in stereo reconstruction. The reason for calibrating the camera is to obtain the intrinsic (focal length and camera center) and extrinsic (transformation between the object coordinate frame and camera centered coordinate frame) parameters of the camera which serve as input data for the stereo reconstruction algorithm. This chapter explains the process of estimating the two categories of parameters through calibration, the stereo reconstruction technique, and its algorithms to estimate depth.

%----------------------------------------------------------------------------------------

\section{Pinhole Model}
The understanding of a pinhole camera model is important to the calibration process as it describes the relationship between the coordinates of a point in world and its projections on the image plane which is the basic idea on which modern-day cameras are being designed. It is the simplest camera model with an infinitely small pinhole aperture through which only a single ray from an object can enter from any direction. The rays entering this pinhole form an inverted image of the object in the world on a plane located at $Z_{ph}=-f$(behind the aperture) called the \textit{image plane} as shown in figure \ref{fig:pinhole}.
\begin{figure}[h!]
\centering
\includegraphics[width=75mm,height = 75mm,keepaspectratio]{pinhole_first.eps}
\caption{Pinhole Model}
\label{fig:pinhole}
\end{figure}
 The pin hole camera played a significant role in the renaissance art and in fact, the understanding of it existed even before the renaissance period around eleventh century. Alhazan (965 CE – 1039 CE) described the visible object as collection of point sources, each of which emitted rays in all directions that could be individually analysed. In his Perspectiva (the thirteenth century Latin translation of his Kitab al-manazir), he accounted and presented the principles under lying the inverted, reversed, and clear images that, for example, candles on one side of a room would make on a dark surface situated behind a small aperture an inverted image of itself \cite{wade2001eye}. And in the fifteenth century, Leonardo da Vinci, the most notable renaissance polymath, described the process of image formation using small holes. His drawings represented cones of light i.e., light rays from an object converging on a pinhole and diverging on the other side to form an inverted image of the object. He also discussed that objects reflect light rays in all directions, and that images will be formed by the passage of light rays through a small hole (pinhole) onto a screen, \cite{hammond1981camera}. In deed, the observation of a scene through a pinhole aided the artists in sketching the scene with realism i.e., the artists were able to sketch the perspective view of the scene using pinholes. This, in turn, aided in representing the three-dimensional space in two-dimensional drawings and also gave an idea of the viewpoint of the artist to the art observer. One of the famous paintings to have incorporated the perspective viewed from a pinhole is “A View of Delft” by Vermeer, a seventeenth century Dutch painter, which is based on a panoramic view of the ancient-day Delft supposed to have been captured inside an apartment room through a pinhole \cite{hammond1981camera}.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{vermeer.pdf}
\caption{A View of Delft by Johannes Vermeer, \\ courtesy: \url{http://www.mauritshuis.nl}}
\label{fig:delft_view}
\end{figure}
 As the understanding of pinhole imaging matured, not only artists but astronomers also used pin holes to capture eclipses for their scientific studies before the advent of telescopes \cite{hammond1981camera}. And in the years after the renaissance period, there was heightened interest among not only artists but scholars from scientific community to study the process of image formation through pinhole which paved the way for the invention of photographic cameras and the rest is history.


For simplicity, a virtual image plane at $Z_{ph}=f$, in figure \ref{fig:pinhole_2} is considered and the pinhole aperture also called the \textit{camera center}, $c$ is considered to be the origin of the euclidean coordinate system.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{pinhole_second.pdf}
\caption{Pinhole Image Formation at $Z_{ph}=f$}
\label{fig:pinhole_2}
\end{figure}

A point in world, $\textbf{X} = (X,Y,Z)$, considered to be in the pinhole camera coordinates, through a transformation $\mathbf{W}$ between the world coordinates and pinhole camera coordinates,  is mapped to a 2D-point, $\textbf{x}=(x,y)$, on the image plane through a line joining the point $\textbf{X}$ to the camera center, $c$. By similar triangles, in figure \ref{fig:pinhole_1}, the coordinates of the point $\textbf{X}$ on the image plane is given by $\left(\dfrac{fX}{Z},\dfrac{fY}{Z}\right)$. The line from the camera center, $c$, which is perpendicular to the image plane is called the \textit{principal/optical axis} of the camera and the point of intersection of the principal axis with the image plane is called the \textit{principal point}, $\textbf{P}$. And the plane at $Z_{ph}=c$ or the camera center which is parallel to the image plane is called the \textit{principal plane}.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{pin_hole_1.pdf}
\caption{Pinhole Geometry}
\label{fig:pinhole_1}
\end{figure}

The transformation between the world and image points in homogenous coordinates can be expressed in matrix form as
\begin{equation}
\begin{bmatrix}
x\\y\\w
\end{bmatrix} = \begin{bmatrix}
f&0&0&0\\
0&f&0&0\\
0&0&1&0\\
\end{bmatrix}\begin{bmatrix}
X\\Y\\Z\\1
\end{bmatrix}
\end{equation}
where the points on the image plane is given by $\left(\dfrac{x}{w},\dfrac{y}{w}\right)$.
In the above equation the origin of the image plane is considered to be at the principal point of the camera. In practice, it is not true. Also, the pixels are typically rectangular and not square. Therefore, the equation is modified as
\begin{equation}
\begin{bmatrix}
x\\y\\w
\end{bmatrix}=\begin{bmatrix}
f_x&0&c_x&0\\
0&f_y&c_y&0\\
0&0&1&0
\end{bmatrix}\begin{bmatrix}
X\\Y\\Z\\1
\end{bmatrix}
\end{equation}
where $(f_x,f_y)$ are the focal length along the x and y-pixel coordinate axes and $(c_x,c_y)$  are the principal point offsets. These four parameters i.e., $f_x,f_y,c_x,c_y$ are camera specific and not dependent on the scene viewed. Therefore, they are categorized as intrinsic parameters.

\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{pin_hole_principal.pdf}
\caption{Pinhole Geometry with Camera Center Offsets $(c_x,c_y)$}
\label{fig:pinhole_principal}
\end{figure}



%-------------------------------------------

\section{Estimation of Intrinsic and Extrinsic Parameters}\label{section 2.2}
The calibration application used in this work is based on Zhang's method \cite{zhang} which uses a planar chessboard pattern. The mapping of object points from one plane to another is known as \textit{planar homography} and the estimation of planar homography is essential since the calibration pattern used  is a planar chessboard. The estimated homography will provide the mapping of points on the chessboard plane to the imager plane of the camera. The points on the chessboard plane and their correspondences on the imager plane are considered in homogenous coordinates to formulate the homography as matrix multiplication. A point $\mathbf{X}=(X,Y,Z)$ on the chessboard plane and its corresponding point on the imager $\mathbf{x}=(x,y)$ are considered in homogenous coordinates to proceed with the formulations.
 \begin{equation}
 \bar{\mathbf{X}} = \begin{bmatrix}
 X &Y &Z &1
 \end{bmatrix}^T
\end{equation}  
\begin{equation}
\bar{\mathbf{x}}=\begin{bmatrix}
x &y &1
\end{bmatrix}^T
\end{equation}
The planar homography is then described as 
\begin{equation}
\bar{\mathbf{x}}=s\mathbf{H}\bar{\mathbf{X}}
\end{equation}where $s$ is an arbitrary scale factor upto which the homography is defined.
The homography $\mathbf{H}$ can be decomposed into two parts: a physical transformation $\mathbf{W}$ relating the chessboard's coordinates to the camera coordinates and the camera matrix $\mathbf{K}$ which projects the objects in the camera coordinates onto the image plane.
The physical transformation and the camera matrix are given by
\begin{equation}
\mathbf{W} = \begin{bmatrix}
\mathbf{R} &\mathbf{t}
\end{bmatrix}\label{eqn:2.6}
\end{equation}
\begin{equation}
\mathbf{K} = \begin{bmatrix}
f_x &0 &c_x \\
0 &f_y &c_y \\
0 &0 &1\\
\end{bmatrix}
\end{equation}
It has been mentioned in the pinhole model that the coordinates of the object are considered in camera coordinates through a transformation, $\mathbf{W}$, from the object coordinate system. In equation \ref{eqn:2.6}, $\mathbf{R}$ is a rotation matrix describing the rotation between the object coordinate and the camera coordinate systems about an axis defined by a unit vector $\mathbf{r}$ and angle $\theta$. This transformation $\mathbf{R} \leftrightarrow \mathbf{r}$ is achieved using the \textit{Rodrigues}' formula and $\mathbf{t}$ is the translation between the two frames of reference. The transformation i.e., extrinsic parameters has been represented as axis-angle \cite{spong2006robot} in chapter \ref{Chapter3}.
\begin{equation}
\begin{aligned}
\theta =& cos^{-1}\left(\dfrac{\Tr(\mathbf{R})-1}{2}\right)\\
\mathbf{r} =& \dfrac{1}{sin \theta} \begin{bmatrix}
R_{32}-R_{23}\\R_{13}-R_{31}\\R_{21}-R_{12}
\end{bmatrix}\\
\end{aligned}
\end{equation}
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{transform_1.pdf}
\caption{Transformation from World Coordinate System to Camera Coordinate System}
\label{fig:transform_1}
\end{figure}


The points in the camera coordinate system are then projected onto the imaging plane using the camera matrix $\mathbf{K}$. 
\begin{equation}
\bar{\mathbf{x}} = s\mathbf{K}\mathbf{W}\bar{\mathbf{X}}
\end{equation}
The chessboard is assumed to be on $XY$ plane ie., $Z=0$ and therefore the relation could be simplified to
\begin{equation}
\begin{bmatrix}
x\\y\\1
\end{bmatrix}=s\mathbf{K}\begin{bmatrix}
\mathbf{r_1}&\mathbf{r_2}&\mathbf{r_3}&\mathbf{t}
\end{bmatrix}\begin{bmatrix}
X\\Y\\0\\1
\end{bmatrix}
\end{equation}
The column 3 of the rotation matrix $r_3$ is then not required
\begin{equation}\label{eq:1.10}
\begin{bmatrix}
x\\y\\1
\end{bmatrix}=s\mathbf{K}\begin{bmatrix}
\mathbf{r_1}&\mathbf{r_2}&\mathbf{t}
\end{bmatrix}\begin{bmatrix}
X\\Y\\1
\end{bmatrix}
\end{equation}
Therefore, the homography matrix, as mentioned before, is defined as a composition of the camera intrinsics and the physical transformation
\begin{equation}\label{eqn:2.12}
\mathbf{H} = s\mathbf{K}\begin{bmatrix}
\mathbf{R}&\mathbf{t}
\end{bmatrix}
\end{equation}
The equation \ref{eq:1.10} can be written as $\mathbf{L}\mathbf{u} = 0$;
\begin{equation}
\begin{bmatrix}
X_1&Y_1&1&0&0&0&-x_1X_1&-x_1Y_1&-x_1\\
0&0&0&X_1&Y_1&1&-y_1X_1&-y_1Y_1&-y_1\\
.&.&.&.&.&.&.&.&.\\
.&.&.&.&.&.&.&.&.\\
X_n&Y_n&1&0&0&0&-x_nX_n&-x_nY_n&-x_n\\
0&0&0&X_n&Y_n&1&-y_nX_n&-y_nY_n&-y_n\\
\end{bmatrix}\mathbf{u}=0
\end{equation}
where $\mathbf{L}$ is the $n\times 9$ matrix and $\mathbf{u} = \begin{bmatrix}
P_0&P_1&P_2&P_3&P_4&P_5&P_6&P_7&P_8
\end{bmatrix}'$ is the vector form of 
\begin{equation}\label{eqn:2.14}
\mathbf{H}=\begin{bmatrix}
P_0&P_1&P_2\\
P_3&P_4&P_5\\
P_6&P_7&P_8
\end{bmatrix}
\end{equation}
The solution to homography is given by the right singular vector of $\mathbf{L}$ associated with the smallest singular value spanning the null space of $\mathbf{L}$.
\begin{equation}
\begin{aligned}
&\mathbf{L} = \mathbf{U} \mathbf{\Sigma} \mathbf{V}\\
&\mathbf{u} = \mathbf{V}(:,9)
\end{aligned}
\end{equation}
Since the singular values are in descending order, the solution to homography is the last column of the matrix $\mathbf{V}$\\

The intrinsic parameters i.e., focal length, $f_x,f_y$, principal point, $c_x,c_y$, define the camera's inherent characteristics and the extrinsic parameters define the pose of the chessboard with regard to the camera centre which is given by three rotation parameters, $(\psi,\phi,\theta)$, and the translation along three axes, $t_x,t_y,t_z$ for every image of the chessboard viewed. Therefore, there are 10 parameters that need to be solved for every view of the chessboard. Consider $N$ as the number of chessboard corners required per view and $M$ as the number of chessboard views to express the constraints and hence determine the number of chessboard views required to estimate the parameters. Since for every corner $N$ there are two pieces of information i.e., the $x$ and $y$ pixel coordinates, the constraint is defined as $2NM$ for $M$ views. And the number of parameters that has to be estimated is 4 intrinsic parameters (irrespective of the number of chessboard views $M$) and the 6 extrinsic parameters for every chessboard view, $6M$. Therefore,
\begin{equation}
\begin{aligned}
2NM \geq & 4+6M\\
(N-3)M \geq & 2\\
\end{aligned}
\end{equation}
From the inequality above, 5 corners i.e., $N=5$ and one chessboard view $M=1$ is enough to estimate the 10 parameters. But for every view 4 corners is all that is required to express everything in a planar perspective. And, additional corners do not have any further influence. Thus, considering the number of corners as a constant i.e., $N=4$, the inequality evolves as
\begin{equation}
(4-3)M \geq 2
\end{equation}
Therefore atleast 2 views of the chessboard, $M=2$, is required to estimate the parameters. However, to obtain good results, 10-15 images of large chessboard $(10\times 11)$ with each cell measuring atleast 1 inch are used and enough movement of the chessboard across the image plane has to be ensured.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{calib_images.eps}
\caption{Chessboard Calibration Pattern}
\label{fig:calib_images}
\end{figure}


If $\mathbf{H}$, in equation \ref{eqn:2.14} and equation \ref{eqn:2.12}, is organized as column vectors,
\begin{equation}
\begin{bmatrix}
\mathbf{h_1}&\mathbf{h_2}&\mathbf{h_3}
\end{bmatrix}=s\mathbf{K}\begin{bmatrix}
\mathbf{r_1}&\mathbf{r_2}&\mathbf{t}
\end{bmatrix}
\end{equation}
The above equation in terms of rotation matrix $\mathbf{R}$'s columns $\mathbf{r}_1$, $\mathbf{r}_2$ and $\mathbf{t}$ is 
\begin{equation}
\begin{aligned}
&\mathbf{r_1} = \lambda \mathbf{K}^{-1}\mathbf{h_1}\\
&\mathbf{r_2} = \lambda \mathbf{K}^{-1}\mathbf{h_2}\\
&\mathbf{t} = \lambda \mathbf{K}^{-1}\mathbf{h_3}
\end{aligned}\label{eqn:2.18}
\end{equation}
where $\lambda = \frac{1}{s}$. 
The vectors $r_1$ and $r_2$ are orthonormal and it provides two constraints i.e., the dot product of the vectors should equal zero and the length of the vector should be unity.

\begin{equation}
\mathbf{r}_1^T\mathbf{r}_2 = 0\label{eqn:2.19}
\end{equation}
\begin{equation}
\|\mathbf{r}_1\|=\|\mathbf{r}_2\| =1
\end{equation}
Substituting $\mathbf{r}_1$ and $\mathbf{r}_2$ in the above equation \ref{eqn:2.19} ,
\begin{equation}\label{eq:1.14}
\mathbf{h}_1^T \mathbf{K}^{-T}\mathbf{K}^{-1}\mathbf{h}_2 = 0
\end{equation}

\begin{equation}\label{eq:1.15}
\mathbf{h}_1^T \mathbf{K}^{-T}\mathbf{K}^{-1}\mathbf{h}_1 = \mathbf{h}_2^{T}\mathbf{K}^{-T}\mathbf{K}^{-1}\mathbf{h}_2
\end{equation}
The closed from solution for the camera intrinsic parameters can be derived from the above conditions. The term $\mathbf{K}^{-T}\mathbf{K}^{-1}$ is simplified in terms of matrix $\mathbf{B}$ before proceeding with the derivation
\begin{equation}
\mathbf{B} = \mathbf{K}^{-T}\mathbf{K}^{-1} = \begin{bmatrix}
B_{11}&B_{12}&B_{13}\\B_{21}&B_{22}&B_{23}\\
B_{31}&B_{32}&B_{33}
\end{bmatrix}
\end{equation}
\begin{equation}
\mathbf{B} = \begin{bmatrix}
\dfrac{1}{{f_x^2}}&0&\dfrac{-c_x}{f_x^2}\\
0&\dfrac{1}{f_y^2}&\dfrac{-c_y}{f_y^2}\\
\dfrac{-c_x}{f_x^2}&\dfrac{-c_y}{f_y^2}&
\dfrac{c_x^2}{f_x^2}+\dfrac{c_y^2}{f_y^2}+1
\end{bmatrix}
\end{equation}
Then, the first constraint \ref{eq:1.14} is written as
\begin{equation}
\begin{split}
h_{11}h_{21}B_{11}+(h_{11}h_{22}+h_{12}h_{21})B_{12}+(h_{11}h_{23}+
h_{13}h_{21})B_{13}+...\\h_{12}h_{22}B_{22}+(h_{12}h_{23}+h_{13}h_{22})B_{23}+h_{13}h_{23}B_{33} = 0
\end{split}
\end{equation}
In general vectorized form, the above equation can be expressed as
\begin{equation}
\mathbf{h}_i^T \mathbf{B} \mathbf{h}_j = \mathbf{v}_{ij}^T \mathbf{b} = \begin{bmatrix}
h_{i1}h_{j1}\\h_{i1}h_{j2}+h_{i2}h_{j1}\\
h_{i2}h_{j2}\\h_{i3}h_{j1}+h_{i1}h_{j3}\\
h_{i3}h_{j2}+h_{i2}h_{j3}\\h_{i3}h_{j3}
\end{bmatrix}^T\begin{bmatrix}
B_{11}\\B_{12}\\B_{22}\\B_{13}\\B_{23}\\B_{33}
\end{bmatrix}
\end{equation}
The second constraint \ref{eq:1.15} can also be written in the above fashion. Therefore, the two constraints are expressed as
\begin{equation}
\begin{bmatrix}
\mathbf{v}_{12}^T\\(\mathbf{v}_{11}-\mathbf{v}_{22})^T
\end{bmatrix}\mathbf{b} = 0
\end{equation}
A set of $K$ chessboard images will result in a $2K \times 6$ matrix $\mathbf{V}$ such that $\mathbf{V}.\mathbf{b} = 0$\\

Solving the above system of equations will provide the $\mathbf{B}$ matrix from which the intrinsic parameters are obtained
\begin{equation}
\begin{aligned}
&f_x = \sqrt{\dfrac{\lambda}{B_{11}}}\\
&f_y = \sqrt{\dfrac{\lambda B_{11}}{(B_{11}B_{22}-B_{12}^2)}}\\
&c_x = \dfrac{-B_{13}f_x^2}{\lambda}\\
&c_y = \dfrac{(B_{12}B_{13}-B_{11}B_{23})}{(B_{11}B_{22}-B_{12}^2)}\\
\end{aligned}
\end{equation}
and $\lambda$ is given by $\lambda = B_{33}-(B_{13}^2+c_y(B_{12}B_{13}-B_{11}B_{23}))/B_{11}$\\

Then, the transformation $\mathbf{W}$ is defined as
\begin{equation}
\mathbf{W} = \begin{bmatrix}
\mathbf{r}_1&\mathbf{r}_2&\mathbf{r}_1 \times \mathbf{r}_2&\mathbf{t}\\
&\mathbf{0}&&1
\end{bmatrix}
\end{equation}

\section{Estimation of Distortion coefficients}
The camera lens typically have distortions which occur due to manufacturing. The two major types of distortion are radial and tangential distortions which lead to pin-cushion and barrel effects in the image.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{distorted_undistorted.pdf}
\caption{Barrel Distortion}
\label{fig:calib_distorted}
\end{figure}
 The camera calibration process described, in the previous section, did not account for the lens distortion. But, in general, the camera calibration process is incomplete without compensating for the lens distortion. There are several models in literature that describe the distortion in the lens. The model described by D.C.Brown \cite{brown} has been implemented in the camera calibration application used in this work. If $x_{\mathbf{p}},y_{\mathbf{p}}$ and $x_{dist},y_{dist}$ are considered to be undistorted and distorted pixels respectively then according to Brown's model the distorted pixels in the observed image are undistorted using the polynomial model given by 
\begin{equation}
\begin{bmatrix}
x_{\mathbf{x}}\\y_{\mathbf{x}}
\end{bmatrix} = \left(1+k_1 r^2+k_2 r^4 + k_3 r^6\right)\begin{bmatrix}
x_{dist}\\y_{dist}
\end{bmatrix}+\begin{bmatrix}
2 p_1 x_{dist} y_{dist}+p_2(r^2+2x_{dist}^2)\\
p_1(r^2+2y_{dist}^2)+2 p_2 x_{dist} y_{dist} 
\end{bmatrix}
\end{equation}

In the above model, the terms $k_1$,$k_2$,$k_3$ characterize the radial distortion and the terms $p_1$,$p_2$ characterize the tangential distortion. Considering the camera parameters estimated from the homography and a zero distortion parameter vector as initial guess, all the camera parameters including the distortion vector $k_1,k_2,k_3,p_1,p_2$ are estimated through non-linear least squares minimization whose objective is to minimize the re-projection error i.e., the error due to the difference in the pixel coordinates of corners from 2D image and the 2D re-projections of the physical coordinates of the corners using the camera parameters. Ideally, 3 corners is all that is required to estimate the distortion parameters, but that may not define the distortion correctly. Therefore, a large number of corners from many different views are used to attain a good approximation of the distortion in the camera. Also the uncertainty in the output parameters of the calibration process is estimated using the standard deviation ($1\sigma$) of the re-projection error values.
\section{A Review of stereo geometry and algorithm}
The technique of stereo vision is used in this work to generate the 3D point cloud data of the pottery shard. The stereo vision algorithms used will also differ between the calibrated and the uncalibrated cameras. Since, in our case, the camera is calibrated, only details pertaining to stereo algorithms used by calibrated cameras is discussed. 

\subsection{Stereo Camera Calibration}
The stereo camera set up is essentially two cameras separated along a horizontal (x-axis) distance. Both the cameras should be of the same specifications to avoid differences in the calibration parameters albeit a negligible difference in parameters is possible. Each of the cameras is calibrated exclusively using the calibration technique described in section  \ref{section 2.2}. In addition, the transformation in terms of rotation and translation is determined between the cameras by using the transformations available between the individual camera's coordinates and the chessboard coordinates as described below. 
\begin{equation}
\begin{aligned}
&\mathbf{H}^l_{cb} = \begin{bmatrix}
R^l_{cb}&T^l_{cb}\\\mathbf{0}&\mathbf{1}
\end{bmatrix}\\
&\mathbf{H}^r_{cb} = \begin{bmatrix}
R^r_{cb}&T^r_{cb}\\\mathbf{0}&\mathbf{1}
\end{bmatrix}\\
\end{aligned}
\end{equation}
where $H^l_{cb}$ is the transformation between the left camera and the chessboard coordinates and similarly, $H^r_{cb}$ is the transformation between the right camera and the chessboard coordinates. A point, $\mathbf{X}$, is represented in the right and left camera frames of reference as shown in equation \ref{eqn:3.2}
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{stereo_transform.pdf}
\caption{Transformation between the Left and Right Camera Coordinates}
\label{fig:stereo_transform}
\end{figure}
\begin{equation}\label{eqn:3.2}
\begin{aligned}
&\mathbf{H}^l_r = \mathbf{H}^l_{cb}{\mathbf{H}^r_{cb}}^{-1}\\
&\begin{bmatrix}
\mathbf{X}^l_{\mathbf{X}}\\1
\end{bmatrix}   = \begin{bmatrix}
\mathbf{R}^l_{cb}&\mathbf{T}^l_{cb}\\\mathbf{0}&1
\end{bmatrix}{\begin{bmatrix}
\mathbf{R}^r_{cb}&\mathbf{T}^r_{cb}\\\mathbf{0}&1
\end{bmatrix}}^{-1} \begin{bmatrix}
\mathbf{X}^r_{\mathbf{X}}\\1
\end{bmatrix}\\
&\begin{bmatrix}
\mathbf{X}^l_{\mathbf{X}}\\1
\end{bmatrix}=\begin{bmatrix}
\mathbf{R}^l_{cb}{\mathbf{R}^r_{cb}}^\mathrm{T}\mathbf{X}^r_{\mathbf{X}}+\mathbf{T}^l_{cb}-\mathbf{R}^l_{cb}{\mathbf{R}^r_{cb}}^\mathrm{T}\mathbf{T}^r_{cb}\\1
\end{bmatrix}\\
\end{aligned}
\end{equation}
where $\mathbf{X}^l_\mathbf{X}$ and $\mathbf{X}^r_\mathbf{X}$ are the coordinates of the point $\mathbf{X}$ in the left and right camera coordinates, $\mathbf{H}^l_r$ is the transformation between the right and left camera. The practical implementation of stereo calibration using MATLAB/openCV will be discussed in chapter \ref{Chapter3}.
\subsection{Stereo Triangulation}\label{sec:st}
The estimation of depth from the stereo image pair is generally called triangulation. It can be formulated using a simple stereo geometry assuming that the stereo image pair is rectified and undistorted. Consider a point in world $\mathbf{p}$ projected onto the left and right image planes as $\mathbf{I_L}_\mathbf{p}$ and $\mathbf{I_R}_\mathbf{p}$. The pixel coordinates of the point $\mathbf{p}$ in the left and right image planes are given by $\left(x^l_{\mathbf{p}},y^l_{\mathbf{p}}\right)$ and $\left(x^r_{\mathbf{p}},y^r_{\mathbf{p}}\right)$. The focal lengths $f_l$ and $f_r$ of the stereo pair are considered equal, $f_l = f_r$ and the coordinates of the principal points along the horizontal i.e., $c^l_x$ and $c^r_x$ are also considered to be equal.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{stereo_geometry.pdf}
\caption{Stereo Geometry in XZ-plane}
\label{fig:stereo_geometry}
\end{figure}

Since the image pair is assumed rectified, the y-pixel coordinates of the point $\mathbf{p}$ in the image pair should be equal i.e., $y^l_{\mathbf{p}}=y^r_{\mathbf{p}}$ and the difference which hereinafter referred as \textit{disparity}, $\mathit{d}$ will only be in the x-pixel coordinates of the corresponding points in the image pair $d=x^l_{\mathbf{p}}-x^r_{\mathbf{p}}$. By similar triangles,
\begin{equation}\label{eqn:depth}
\begin{aligned}
&\frac{X_{\mathbf{p}}-B}{Z_{\mathbf{p}}} = \frac{x^l_{\mathbf{p}}}{f}\\
&\frac{X_{\mathbf{p}}}{Z_{\mathbf{p}}} = \frac{x^l_{\mathbf{p}}}{f}\\
&\frac{x^l_{\mathbf{p}}}{f}-\frac{B}{Z_{\mathbf{p}}}=\frac{x^r_{\mathbf{p}}}{f}\\
&Z_{\mathbf{p}} = \frac{fB}{x^l_{\mathbf{p}}-x^r_{\mathbf{p}}}\\
\end{aligned}
\end{equation}
Therefore, the depth of the point $\mathbf{p}$ is inversely proportional to the \textit{disparity} of the point's x-pixel coordinates. Obtaining the coordinates $X_{\mathbf{p}}$ and $Y_{\mathbf{p}}$ is straightforward from the pinhole model.

\subsection{Stereo Rectification}
The equation \ref{eqn:depth} is derived  assuming the image pair to be rectified. But, in general, the image pair is not rectified i.e., y-pixel coordinates of the corresponding features in the image pair are not equal, $y^l_{\mathbf{p}}\neq y^r_{\mathbf{p}}$. Therefore, the image pair has to be rectified to make them coplanar and colinear. The terminologies associated with stereo geometry are defined below \newline
\textbf{Baseline}, $B$ is the line separating the camera centres $c_L$ and $c_R$. \newline
\textbf{Epipoles} are the camera center of the second camera projected on the first camera's image plane and vice versa. Or it could be defined as the points of intersection of the baseline with the image planes, $e_L$ and $e_R$.\newline
\textbf{Epipolar plane} contains the baseline and the point in world $\mathbf{p}$. \newline
\textbf{Epipolar line} is the line segment defined by the points of intersection of the epipolar plane with the image plane. i.e.,${I_L}_{\mathbf{p}}e_L$ and${I_R}_{\mathbf{p}}e_R$.

\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{stereo_geometry_2.pdf}
\caption{Stereo Geometry in the Raw Unrectified Case}
\label{fig:stereo_geometry_2}
\end{figure}

The relationship between projections on the left and right image planes of any point in world $\mathbf{p}$ can be expressed using the above terms. The projection on the left image ${I_L}_{\mathbf{p}}$ can have its correspondence anywhere on the epipolar line ${I_R}_{\mathbf{p}}e_R$ on the right image and vice versa. This is called the \textbf{epipolar constraint}. 

Since the pose of the right camera $(\mathbf{R},\mathbf{t})$ with respect to the left camera has been determined from stereo calibration, the transformation is used to rectify the image pair such that the image pair becomes coplanar. In fact, instead of simply rotating the right camera and aligning with left camera, the rotation matrix $\mathbf{R}$ is split into $\mathbf{R}_l$ and $\mathbf{R}_r$ for the left and right cameras, so that, both the cameras undergo half the rotation intended by $\mathbf{R}$ and become coplanar with the epipoles at infinity.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{stereo_geometry_3.pdf}
\caption{Stereo Geometry in the Rectified Case}
\label{fig:stereo_geometry_3}
\end{figure}

But to make the pair row aligned another rotation matrix has to be defined. Considering the principal point of left image as origin, the direction of epipole, $e_1$ is along the horizontal given by the translation vector $\mathbf{t}=\begin{bmatrix}
-t_x&-t_y&-t_z
\end{bmatrix}$ between the two cameras' centres of projection. Therefore,
\begin{equation}
e_1 = \dfrac{\mathbf{t}}{\|\mathbf{t}\|}
\end{equation} 
The second vector, $e_2$ can be constructed from cross product of $e_1$ and the principal ray $[0,0,1]$ such that the normalized vector $e_2$ is given by
\begin{equation}
e_2 = \dfrac{\begin{bmatrix}
-t_y&t_x&0
\end{bmatrix}}{\sqrt{t_y^2+t_x^2}}
\end{equation}
And finally, the third vector is an obvious crossproduct of $e_1$ and $e_2$ i.e., $e_3 = e_1\times e_2$ which together with the vectors $e_1$ and $e_2$ form the rotation matrix which will rotate the left camera about its center of projection.
\begin{equation}
\mathbf{R}_{rectification} = \begin{bmatrix}
e_1\\e_2\\e_3
\end{bmatrix}
\end{equation}
Therefore, the comprehensive rotation matrices that will make the image pair coplanar as well as collinear are given by
\begin{equation}
\begin{aligned}
\mathbf{R}_l^{\prime} =& \mathbf{R}_{rectification}\mathbf{R}_l\\
\mathbf{R}_r^{\prime} =& \mathbf{R}_{rectification}\mathbf{R}_r
\end{aligned}
\end{equation}

\subsection{Stereo Matching}
\subsubsection{Sum of Absolute Differences}
The disparity computation is essentially the most important and difficult part in depth estimation using stereo vision. There are many algorithms to compute stereo disparity \cite{scharstein} but in this work the Sum of Absolute Differences (SAD) is used to compute the disparity. It is a window-based method where the size of the pixel-window and the disparity search range influence the quality of the disparity image. For a pixel ${I_L}_{\mathbf{p}}$ selected in the left image, a window of appropriate size is selected to mask the neighbourhood of ${I_L}_{\mathbf{p}}$. Then, a window of same size scans through the disparity range along the epipolar line in the right image while aggregating a cost $C_{SAD}$ which is the sum of absolute intensity differences between the left image window and the the right image window for each pixel increment in the disparity range of the right image. The cost, $C_{SAD}$ is given by
%\begin{eqnarray}
\begin{equation}
C_{SAD} = 
min_{d=dmin}^{dmax}\sum\limits_{i=-\frac{m}{2}}^{\frac{m}{2}}\sum\limits_{j=-\frac{m}{2}}^{\frac{m}{2}}|I_L\left({x^l}_{\mathbf{p}}+i,y^l_{\mathbf{p}}+j\right)-I_R\left(x^r_{\mathbf{p}}+i+d,y^r_{\mathbf{p}}+j\right)|
\end{equation}
%\end{eqnarray}
The pixel, ${I_R}_{\mathbf{p}}$, in the right image's disparity range that corresponds to the minimum cost is considered to be the match for the pixel ${I_L}_{\mathbf{p}}$ in the left image. The difference in the x-pixel values of ${I_L}_{\mathbf{p}}$ and ${I_R}_{\mathbf{p}}$ i.e., $x^l_{\mathbf{p}}-x^r_{\mathbf{p}}$  is the disparity.

\begin{figure}[h!]
\centering
\includegraphics[width=175mm,height = 175mm,keepaspectratio]{thesis_SAD_picture.pdf}
\caption{Illustration of Finding the Matching Pixel in the Right Image to a Pixel of Interest in the Left Image }
\label{fig:thesis_SAD}
\end{figure}

The Sum of Absolute Differences is a simple strategy to compute the disparity by comparing the texture in the stereo pair provided by the window. But as always with any algorithm, SAD does have its shortcomings when computing disparities in texture less areas, foreshortened objects, and occluded regions (occlusions are pattern available in one view and not in the other view). To overcome the problem of computing erroneous disparity values in the above cases, there are few constraints incorporated to SAD to eliminate the influence of the affected pixels. Before discussing the constraints, the SAD results obtained on a ground truth dataset will be discussed. The rectified grayscale images are used for the SAD computation and one such ground truth stereo pair famously known as Tsukuba data set \cite{martin} is used for discussion. 
\begin{figure}[h!]

\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=9cm,height=9cm,keepaspectratio]{stereo_left.eps}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=9cm,height=9cm,keepaspectratio]{stereo_right.eps}
\end{subfigure}
\caption{Tsukuba Stereo Pair}
\label{fig:tsukuba_stereo}
\end{figure}
Since the stereo pair is rectified and already under the epipolar constraint, the disparity search is 1D, ie., $d\in \mathbb{R}$ along the horizontal lines, and is based on the calculation of SAD scores in the disparity search range. The quality of the disparity image computed, as mentioned before, will depend on the window-size and the disparity search range. The disparity images for different window-sizes and search ranges are shown below.
\begin{figure}[h!]
\centering
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.8cm,height=5.8cm,keepaspectratio]{disparity_3x3.pdf}
\end{subfigure}
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.8cm,height=5.8cm,keepaspectratio]{disparity_5x5.pdf}
\end{subfigure}
\caption{Disparity Images for Different Window Sizes and Search Ranges}
\end{figure}
\begin{figure}[h!]
\centering
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.5cm,height=5.5cm,keepaspectratio]{disparity_9x9.pdf}
\end{subfigure}
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.5cm,height=5.5cm,keepaspectratio]{disparity_ground.eps}
\end{subfigure}
\caption{Disparity Image with 9x9 Window and Ground Truth Disparity}
\end{figure}
In the figures, the disparity image with the smallest window has more disparity information especially at depth discontinuities or in other word object boundaries in the intensity image are preserved in the disparity. But it is also noisy with some random depth information i.e., the disparity is not smoothly varying across some regions. On the contrary, the disparity image with the larger window tends to be smoothly varying but it lacks information at depth discontinuities or object boundaries and if the window size is increased further the generated disparity image will altogether lose information on a fine region like the camera handle in the Tsukuba dataset. The disparity search range is also crucial to the quality of the disparity image. If the disparity search range is large particularly in an image where the objects of interest are distant from the camera and thus the horizontal shift between the objects' projections across the stereo pair is small, there are chances of false correspondences since the disparity computation is based on the texture that is masked by the window in a position and not based on the semantics of the image. Therefore, the disparity search should be conservative based on the considered image to eliminate the possibility of false correspondences.

\subsubsection{Constraints}

As mentioned in the previous section, there are few constraints which when incorporated in the stereo matching algorithm improve the disparity computation. Only two constraints which are of most importance for this work will be discussed.

\paragraph{Uniqueness Constraint}
Ideally, every pixel in the left image of the stereo pair should at most have one correspondence in the right image and vice versa. If a pixel has in one of the images more than one correspondence in the other image, then that pixel is deemed invalid for depth computation. There are many reasons for this problem of which matching texture less regions is the most common reason since there is no distinctive feature that can be matched on one to one basis in the stereo pair. Incorporating this constraint will drastically improve the disparity map, and in turn the reconstructed 3D point cloud will have reasonable depth values. There are many ways to employ this constraint. 
The formulation used in this work is adapted from MATLAB's implementation where two minimum values of the $C_{SAD}$ vector are considered. The first one is the global minimum $D$ at the vector index, $d$ from the vector and the second minimum is the global minimum $D^{\prime}$ after excluding the vector indexes $d-1,d,d+1$. Then if \begin{equation}
D^{\prime} < D(1+0.01*\text{uniqueness threshold value})
\end{equation}then, that pixel is labelled invalid. The number of pixels that will be labelled as invalid in a disparity image depends on the uniqueness threshold value selected, higher its value higher the number of invalid pixels. The value has to be carefully chosen depending on the image so as to avoid unnecessary elimination of valid pixels.

\paragraph{Consistency Check}
The false correspondences also arise from occlusions where a texture is available in one of the images but not the other. Therefore it is very important to identify such regions and label them as occluded. This is implemented by generating a pair of disparity images i.e., one with correspondence search in the right stereo image and the other with the correspondence search in the left stereo image. Now, if a pixel is selected in one of the disparity image pair, the disparity value of that pixel should point to the pixel coordinates in the second disparity image which will also have the same disparity value ie., the disparity value of this pixel in the second image will again point to its counterpart in the first image, thus ensuring one to one correspondence, $D_{L_\mathbf{p}}(x_{\mathbf{p}}^l,y_{\mathbf{p}}^l)=d_\mathbf{p}=D_{R_{\mathbf{p}}}(x_{\mathbf{p}}^l-d_{\mathbf{p}},y_{\mathbf{p}}^r)$ where$D_{L_{\mathbf{p}}}$ and $D_{R_{\mathbf{p}}}$ are the disparity images corresponding to the left image coordinates and right image coordinates and $x_{\mathbf{p}}^l-d_{\mathbf{p}}=x_{\mathbf{p}}^r$. If any of the pixels violate this condition then they will be considered invalid and labelled as occlusion. 
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{consistency_map}
\caption{Disparity Map after Consistency Check}
\label{fig:consistency_map}
\end{figure}
In figure \ref{fig:consistency_map}, the occluded pixels can be noticed as holes across the image, especially a large occlusion can be seen to the left of the statue and left of the lamp, the regions which are, indeed, not available in the right camera view and therefore labelled as occlusion.

\subsection{Depth Estimation}

Once the disparity image of the observed scene is computed, the depth of the scene can be estimated using the equations of section \ref{sec:st} and the calibration parameters estimated. In fact, a unique disparity to depth reprojection matrix is used to calculate the depth values of the pixels of interest in the scene. 
\begin{equation}
\mathbf{Q}\begin{bmatrix}
x_\mathbf{p}^l\\y_\mathbf{p}^l\\d_\mathbf{p}\\1
\end{bmatrix}=\begin{bmatrix}
X_\mathbf{p}\\Y_\mathbf{p}\\Z_\mathbf{p}\\W
\end{bmatrix}
\end{equation}where $\mathbf{Q}=\begin{bmatrix}
1&0&0&-c_x^l\\
0&1&0&-c_y^l\\
0&0&0&f\\
0&0&\dfrac{-1}{t_x}&\dfrac{(c_x^l-c_x^r)}{t_x}
\end{bmatrix}$
is the disparity to depth re-projection matrix consisting of the camera parameters.
\section{Summary}
The Camera calibration process provides the camera parameters to re-project the pixel coordinates to 3D coordinates. And the stereo correspondence estimation provides the disparity of the pixels of interest in the image which is required to calculate the depth of the points from the camera center. In chapter \ref{Chapter3}, the practical implementation is discussed and the estimated parameters are tabulated.