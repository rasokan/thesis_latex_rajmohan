% Chapter 2

\chapter{Pottery Bowl Reconstruction} % Main chapter title

\label{Chapter4} % For referencing the chapter elsewhere, use \ref{Chapter1} 

\lhead{Chapter 4. Pottery Bowl Reconstruction} % This is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------


This chapter will discuss the origins of the pottery shard that was considered for reconstruction in chapter \ref{Chapter3} and the technique used to identify the parameters of the geometric primitives fitted to the edge point cloud data of the shard which is a part of a rotationally symmetric object (bowl).

\section{Teotihuacan - A precolumbian civilization}
Teotihuacan is an ancient urban center 40 Kilometers north east of modern-day Mexico City. This ancient city at its peak, between 250 CE and 650 CE, dominated the central Mexican highlands and had far flung relationships throughout Mesoamerica which includes the countries between El Salvador and Mexico, south of its northern desert. 
Teotihuacan as an archaeological site is massive in size covering roughly 21 square Kilometers with three large pyramidal structures the largest of which stands over 200 feet in height and 716 feet on a side. These are the pyramid of the Sun (the largest), the pyramid of the moon at the top of the street of the dead and the Temple of the Feathered Serpent near the market and political center of the city. The city itself shows signs of overwhelming urban planning which includes over 2000 residential compounds, streets, plazas, platforms and walls all oriented 15 degrees 25 minutes east of true north. While the city began around 200 BCE this impressive grid structural alignment appears around 250CE and continues to the end of the city which is hallmarked by the center being heavily burned sometime between 600 and 650 CE \cite{cowgill}.
The bowl being studied was found at Teotihuacan roughly 2 Kilometers northeast of the pyramid of the Moon in a residential context. This is important because other pottery related to this shallow bowl are found above the period of destruction of the ancient city. It was believed by many archaeologists thought that it was confined to the small area of San Francisco Mazapan a Kilometer and a half east of the Pyramid of the Sun and linked the destruction of Teotihuacan with the rise of the next civilization, Tula Hidalgo.
Since the forties and fifties archaeologists have found another ceramic complex between Mazapan and Metepec pottery (the last ceramics associated with ancient Teotihuacan). What then is the role of the people who made the Mazapan pottery? The almost complete bowl that is being studied will help the archaeologists in answering this question. In short, most of the ceramic evidence that archaeologists use for defining ceramic periods and putting together the lives of ancient peoples are unmarked, badly worn pieces of pottery (sherds, shards in England). An important part of the puzzle is being able to tell how many of each type of pottery are the archaeologists dealing with to form an impression of the number of people lived in a specific place and their role. To be able to take small sherds and resurrect the complete vessel will go a long way towards answering these questions as well as others such as “\textbf{what is the proportional distribution among domestic wares and ceremonial wares}?”, “\textbf{Do two different shards represent variants of one type or two completely different cultures?}”  Besides these examples this research would help in creating a 3D printed replica of a fine ceremonial bowl to do research where use of the original would destroy it.

\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{mazapan_pottery.pdf}
\caption{Mazapan Pottery \cite{mazapan}}
\label{fig:mazapan_pottery}
\end{figure}

\section{Parameter Identification}
One of the widely used geometric primitives in computer vision applications is circle. The idea behind the reconstruction of the complete pottery is by fitting layers of circles to the reconstructed edge point cloud from chapter \ref{Chapter3} along the Z-axis thereby obtaining an approximate representation of the pottery bowl from the layers of circles.

\subsection{Circle Fitting}
 First, circle fitting based on Kasa's method \cite{kasa} was attempted, but mostly the output parameters returned were short of the actual values. This disadvantage in using Kasa's method is shown using synthetic data and the Taubin's circle fitting \cite{taubin} is explained which returns close to actual parameter values. 
\subsubsection{Kasa's method}
As with any conic function, the circle fitting is described as the minimization of the following circle equation,
\begin{equation}
P_{Kasa} = \sum_{i=1}^{N}[(x_i-a)^2+(y_i-b)^2-R^2]^2
\end{equation}
where $N$ is the number of points $(x_i,y_i)$ in the dataset, $a,b,R$ are the estimated center coordinates and radius. A simple change of parameters is used to avoid the non linearity in the derivatives of the original equation. Therefore,
\begin{equation}
P_{Kasa} = \sum_{i=1}^{N}(z_i+Bx_i+Cy_i+D)^2
\end{equation}
where $B=-2a;C=-2b;D=a^2+b^2-R^2$ and also to use minimum number of terms $z_i=x_i^2+y_i^2$.
Differentiating the original equation with respect to $a,b,R$ translates to differentiating the new equation with respect to $B,C,D$.
\begin{equation}\label{kasa_der}
\begin{aligned}
\dfrac{\partial P_{kasa}}{\partial B} =& 2\sum_{i=1}^{N}[z_i+Bx_i+Cy_i+D]x_i=0\\
=&\sum x_i^2B+\sum x_iy_iC+\sum x_iD = -\sum x_i Z\\
\dfrac{\partial P_{kasa}}{\partial C} =&\sum x_iy_iB+\sum y_i^2C+\sum y_iD=-\sum y_iZ\\
\dfrac{\partial P_{kasa}}{\partial D} = &\sum x_iB+\sum y_iC+ND = -\sum z_i\\
\end{aligned}
\end{equation}
The coefficients of the above system of equations can be defined in matrix form as $\mathbf{P}_{kasa}^T \mathbf{P}_{kasa}$ where $\mathbf{P}_{kasa} = \begin{bmatrix}
x_1&y_1&1\\.&.&.\\x_N&y_N&1
\end{bmatrix}$.
Formulating the equation \ref{kasa_der} as \begin{equation}\mathbf{P}_{kasa}^T\mathbf{P}_{kasa}\mathbf{u} = -\mathbf{P}_{kasa}^T \mathbf{z}
\end{equation}, where $\mathbf{u}=[B,C,D]^T$ and $\mathbf{z}=[z_i,.,.,z_N]^T$, the parameter vector $\mathbf{u}$ is derived as
\begin{equation}
\mathbf{u} = -\mathbf{P}_{kasa}^{-}\mathbf{z}
\end{equation}
where $\mathbf{P}_{kasa}^{-}$ is the pseudo-inverse. While the above implementation is from the work of Chernov \cite{chernov}, an alternative closed form solution from Kasa [\cite{kasa},\cite{rusu2003classical}] can also be implemented instead of a pseudo-inverse calculation. The derivations are skipped and only the closed form equations for the center and radius have been provided in this document.
And, the center and radius are given by
\begin{equation}
\begin{aligned}
a =& \dfrac{\sum_{\theta}\sum_{\gamma}-\sum_{\epsilon}\sum_{\beta}}{\sum_{\alpha}\sum_{\gamma}-\sum_{\beta}^2}\\
b =& \dfrac{\sum_{\alpha}\sum_{\epsilon}-\sum_{\beta}\sum_{\theta}}{\sum_{\alpha}\sum_{\gamma}-\sum_{\beta}^2}\\
R =& \sqrt{a^2+b^2+\dfrac{1}{N}\left[\sum_{i=1}^N x_i^2 -2a \sum_{i=1}^N x_i + \sum_{i=1}^N y_i^2 -2b \sum_{i=1}^N y_i\right]}\\
\end{aligned}
\end{equation}
where,
\begin{equation}
\begin{aligned}
\sum \nolimits_{\alpha} =& 2[(\sum_{i=1}^N x_i^2)-N\sum_{i=1}^N x_i^2]\\
\sum \nolimits_{\beta}=& 2[(\sum_{i=1}^N x_i)(\sum_{i=1}^N y_i)-N\sum_{i=1}^N x_iy_i]\\
\sum \nolimits_{\gamma} =& 2[(\sum_{i=1}^N y_i)^2-N\sum_{i=1}^N y_i^2]\\
\sum \nolimits_{\theta}=& (\sum_{i=1}^N x_i^2)(\sum_{i=1}^N x_i)-N(\sum_{i=1}^N x_i^3)+(\sum_{i=1}^N x_i)(\sum_{i=1}^N y_i^2)-N(\sum_{i=1}^N x_i y_i^2)\\
\sum \nolimits_{\epsilon} =& (\sum_{i=1}^N x_i^2)(\sum_{i=1}^N y_i)-N(\sum_{i=1}^N y_i^3)+(\sum_{i=1}^N y_i)(\sum_{i=1}^N y_i^2)-N(\sum_{i=1}^N x_i^2 y_i)\\
\end{aligned}
\end{equation}

 Now, estimating the parameters of a circle fitted to data points sampled through half the circle whose center is at $[7,6]$ and radius $\text{r = }3.5$ returns exactly the actual parameters. 
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{kasa_fit_aug_6.pdf}
\caption{Circle Estimated by Kasa's Method}
\label{fig:pi_kasa}
\end{figure}
This is not a challenge as all the points were indeed lying on a semi circle. But the Kasa's method struggles(in figure \ref{fig:pi_taubin}) when only data from an arc of $45^{\circ}$ distorted with a gaussian noise with variance $\sigma^2=0.1$ is used. It returns parameters of a circle, $\text{center = }[8.0625,6.4248]\text{ and radius = }2.3487$, much smaller than the actual circle whereas Taubin's method, which will be discussed in the next section, returns parameters, $\text{center = }[7.1732, 6.0426]\text{ and radius = }3.2726$, closer to the actual circle. A detailed analysis of the heavy bias in Kasa's fit is provided by Chernov \cite{chernov} and this is the reason why circle fitting is implemented using Taubin's method.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{taubin_fit_aug_6.pdf}
\caption{Circle Estimated by Kasa's and Taubin's Methods from Points on a Smaller Arc}
\label{fig:pi_taubin}
\end{figure}



\subsubsection{Taubin's Method}
The Kasa's minimization function can be modified as 
\begin{equation}
P_{kasa} = \sum_{i=1}^{N}d_i^2(d_i+2R)^2\label{eqn:5.6}
\end{equation}
where $d_i=r_i-R$ is distance between the point $(x_i,y_i)$ and its closest point on the circle $(a,b,R)$ with $r_i=\sqrt{(x_i-a)^2+(y_i-b)^2}$.
Since $|d_i|<<R$,
\begin{equation}
P_{kasa} = 4R^2\sum_{i=1}^{N}d_i^2
\end{equation}
Also,
\begin{equation}
R^2\approx \dfrac{1}{N}\sum_{i=1}^{N}(x_i-a)^2+(y_i-b)^2
\end{equation}
Therefore, equation \ref{eqn:5.6} can be expressed as
\begin{equation}
P_{kasa}\approx \dfrac{4}{N}\left[\sum_{i=1}^{N}(x_i-a)^2+(y_i-b)^2\right]\left[\sum_{i=1}^{N}d_i^2\right]
\end{equation}
Since the objective here is to minimize $$\sum_{i=1}^{N}d_i^2$$ it can accomplished by minimizing the following
\begin{equation}
P_{taubin}=\dfrac{\sum [(x_i-a)^2+(y_i-b)^2-R^2]}{4N^{-1}\sum [(x_i-a)^2+(y_i-b)^2]}
\end{equation}
Again, by change of parameters $a = -\dfrac{B}{2A}\hspace{2mm}b=-\dfrac{C}{2A}\hspace{2mm}R^2=\dfrac{B^2+C^2-4AD}{4A^2}$,
\begin{equation}
P_{taubin} = \dfrac{\sum [Az_i+Bx_i+Cy_i+D]^2}{N^{-1}\sum [4A^2z_i+4ABx_i+4ACy_i+B^2+C^2]}\label{eqn:5.11}
\end{equation}
Though the above minimization function was proposed by Taubin, the derivations in this work leading to this minimization function is from the work of Chernov \cite{chernov}.

The equation \ref{eqn:5.11} is equivalent to minimizing the function, \begin{equation}\sum_{i=1}^{N}[Az_i+Bx_i+Cy_i+D]^2\label{eqn:5.12}
\end{equation} with the constraint, $4A^2\bar{z}+4AB\bar{x}+4AC\bar{y}+B^2+C^2=1$ where $\dfrac{1}{N}\sum z_i=\bar{z};\dfrac{1}{N}\sum x_i = \bar{x};\dfrac{1}{N}\sum y_i = \bar{y}$ are the first moments. Now to implement the Taubin's method a few more steps are considered. From the minimization function, it is seen,
\begin{equation}
D=-A\bar{z}-B\bar{x}-C\bar{y}\label{eqn:5.13}
\end{equation}
Therefore, the equation \ref{eqn:5.11} can be rewritten as
\begin{equation}
P_{taubin}=\dfrac{\sum [Az_i+Bx_i+Cy_i+D]^2}{B^2+C^2-4AD}
\end{equation}
Now, the objective function expressed in equation \ref{eqn:5.12} is subjected to two constraints, the first given by equation \ref{eqn:5.13} and the second constraint, \begin{equation}
B^2+C^2-4AD=1
\end{equation}
If the data is normalized and centred so that the centroids are $\bar{x}=0,\bar{y}=0$ and thus, $D=-A\bar{z}$. And the minimization function, once again, is modified to
\begin{equation}
P_{taubin} = \sum_{i=1}^{N}[A(z_i-\bar{z})+Bx_i+Cy_i]^2\label{eqn:5.16}
\end{equation} with the constraint also being modified to
\begin{equation}
4\bar{z}A^2+B^2+C^2=1\label{eqn:5.17}
\end{equation}
In Chernov's work, another parameter $A_0=2\bar{z}^{1/2}A$ is introduced to make the further steps simpler. The equations \ref{eqn:5.16} and \ref{eqn:5.17} are then modified as,
\begin{equation}
P_{taubin}=\sum_{i=1}^{N}\left[A_0\dfrac{z_i-\bar{z}}{2\bar{z}^{1/2}}+Bx_i+Cy_i\right]^2
\end{equation}
and constraint as
\begin{equation}
A_0^2+B^2+C^2=1
\end{equation}
Again, representing the minimization function in terms of matrix multiplication as in Kasa's method,
\begin{equation}
P_{taubin} = \|\mathbf{X}\mathbf{u}\|^2
\end{equation}
where $\mathbf{X}=\begin{bmatrix}
(z_1-\bar{z})/(2\bar{z}^{1/2})&x_1&y_1\\.&.&.\\(z_N-\bar{z})/(2\bar{z}^{1/2})&x_N&y_N\\
\end{bmatrix}$ and $\mathbf{u}=[A_0,B,C]^T$ with the constraint \begin{equation}
\|\mathbf{u}\|=1
\end{equation}
The above minimization can be implemented either using Singular Value Decomposition (SVD) or in a Newton based method.
\section{Surface Reconstruction}
The point cloud in figure \ref{fig:shard_recon} is initially fitted with a circle using taubin's method and points from the rim section. The circle parameters returned is plotted on the point cloud data as shown in figure \ref{fig:circle_rim}.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{circle_rim.pdf}
\caption{Circle Fit to Rim Points}
\label{fig:circle_rim}
\end{figure}
The parameters of this circle are estimated as
\begin{equation}
\begin{aligned}
\text{center} = &\hspace{2mm} [2.3995,5.4258]\\
\text{radius} = &\hspace{2mm} 3.1213\hspace{2mm} \text{inches}\\
\end{aligned}
\end{equation}
Assuming the axis of rotation to be normal to the plane at the center point of the rim, which is not true always, the remaining layers of circle are fitted by estimating the radii of the circles using the edge points in the cloud and the axis in a least squares manner.
\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{circle_layers.pdf}
\caption{Layers of Circle Fitted to the Point Cloud}
\label{fig:circle_layers}
\end{figure}
The estimated radii will not be smooth due to the point cloud being not smooth. Therefore, a cubic curve fit is made on the estimated radii to get a smooth curve which will be used to reconstruct the pottery, as in figure \ref{fig:surface_fit} which is the geometric surface estimated for the shard in figure \ref{fig:shard_stereo}. The cubic fit has a sum squared error of $0.2186$, in this case, which is lesser than a quadratic fit (not shown). Also higher order polynomial curves were not selected due to over-fitting of the data.

\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{cub_z_curve.pdf}
\caption{Cubic Curve Fit of Radii}
\label{fig:quadratic_fit}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=150mm,height = 150mm,keepaspectratio]{surface_reconstructions_multiple.pdf}
\caption{Surface Reconstructions of the Pottery with Shard's Edge Point Cloud Overlaid}
\label{fig:surface_fit}
\end{figure}

\section{Comparison with the ground truth data}\label{section:ground_truth}
The ground truth is obtained by estimating the circle parameters of the rim data points from the larger assembly of shards/ major portion of the pottery available to us. The stereo image of the larger assembly is shown in figure \ref{fig:ground_stereo}.
\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{ground_stereo.pdf}
\caption{Stereo Image of the Larger Assembly of Shards from the Pottery}
\label{fig:ground_stereo}
\end{figure}

The corresponding rectified image pair, disparity image, and the estimated circle overlaid on the rim point cloud are shown below
\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{ground_rectified.pdf}
\caption{Rectified Image Pair of the Larger Assembly of Shards from the Pottery}
\label{fig:ground_rectified}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{ground_disparity.pdf}
\caption{Disparity Image of the Larger Assembly of Shards' Rim points \\ Window Size: 31x31; Disparity Range: 330-368 pixels}
\label{fig:ground_disparity}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{ground_truth_2.pdf}
\caption{Circle Estimated from the Larger Assembly of shards' Rim Points}
\label{fig:ground_truth}
\end{figure}

Clearly, the circle's radius estimated from the rim points of the smaller shard in figure \ref{fig:circle_rim} is closer to the radius of the circle estimated from the rim points of the larger assembly in figure \ref{fig:ground_truth} and the percent error is $1.1094 \%$. The small difference in the radii values could be attributed to the orientation error in the smaller shard as there was no proper estimation of the axis of rotation. Therefore, the estimated circle will have a larger or smaller radius compared to the actual radius value depending on the orientation error. The estimation of the rotation axis is very important to eliminate the orientation error. To automatically estimate the rotation axis, there should be enough surface points of the shard from which the axis can be estimated through some geometrical means as in \cite{halir1999automatic}. The acquisition of the surface points from poorly textured surfaces like the shard without any ambiguity in depth can be performed using global stereo matching techniques such as graph cut based stereo matching \cite{kolmogorov2001computing} which use variational methods to estimate the stereo correspondences.

Also, the larger assembly of shards was traced on a graph paper for manual measurements and the diameter was determined to be $6.38$ inches. The figures below show the assembly placed on a graph paper and the corresponding measurements from the traced contour.

\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{gr_1.pdf}
\caption{Larger Assembly of Shard on Graph Paper}
\label{fig:gr_1}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=125mm,height = 125mm,keepaspectratio]{gr_2.pdf}
\caption{Larger Assembly of Shard with the Smaller Shard}
\label{fig:gr_2}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=175mm,height = 175mm,keepaspectratio]{graph.pdf}
\caption{Measurements on the Trace}
\label{fig:graph}
\end{figure}