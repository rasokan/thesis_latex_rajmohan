% Chapter 2

\chapter{A Review of stereo geometry and algorithm} % Main chapter title

\label{Chapter3} % For referencing the chapter elsewhere, use \ref{Chapter1} 

\lhead{Chapter 3. A Review of stereo geometry and algorithm} % This is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------


The technique of stereo vision is used in this work to generate the 3D point cloud data of the pottery shard. The stereo vision algorithms used will also differ between the calibrated and the uncalibrated cameras. Since in our case the camera is calibrated this chapter will only provide details pertaining to stereo algorithms used by calibrated cameras. 

\section{Stereo Camera Calibration}
The stereo camera set up is essentially two cameras separated along a horizontal (x-axis) distance. Both the cameras should be of the same specifications to avoid differences in the calibration parameters albeit a negligible difference in parameters is possible. Each of the cameras is calibrated exclusively using the calibration technique described in chapter  \ref{Chapter2}. In addition, the transformation in terms of rotation and translation is also determined between the cameras by using the transformations available between the individual cameras and the chessboard coordinates as described below. 
\begin{equation}
\begin{aligned}
&\mathbf{H}^l_{cb} = \begin{bmatrix}
R^l_{cb}&T^l_{cb}\\\mathbf{0}&\mathbf{1}
\end{bmatrix}\\
&\mathbf{H}^r_{cb} = \begin{bmatrix}
R^r_{cb}&T^r_{cb}\\\mathbf{0}&\mathbf{1}
\end{bmatrix}\\
\end{aligned}
\end{equation}
where $H^l_{cb}$ is the transformation between the left camera and the chessboard coordinates and similarly $H^r_{cb}$ is the transformation between the right camera and the chessboard coordinates. A point, $\mathbf{X}$, is represented in the right and left camera frames of reference as shown in equation \ref{eqn:3.2}
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{stereo_transform}
\caption{Transformation between the Left and Right Camera coordinates}
\label{fig:stereo_transform}
\end{figure}
\begin{equation}
\begin{aligned}
&\mathbf{H}^l_r = \mathbf{H}^l_{cb}{\mathbf{H}^r_{cb}}^{-1}\\
&\begin{bmatrix}
\mathbf{X}^l_{\mathbf{X}}\\1
\end{bmatrix}   = \begin{bmatrix}
\mathbf{R}^l_{cb}&\mathbf{T}^l_{cb}\\\mathbf{0}&1
\end{bmatrix}{\begin{bmatrix}
\mathbf{R}^r_{cb}&\mathbf{T}^r_{cb}\\\mathbf{0}&1
\end{bmatrix}}^{-1} \begin{bmatrix}
\mathbf{X}^r_{\mathbf{X}}\\1
\end{bmatrix}\\
&\begin{bmatrix}
\mathbf{X}^l_{\mathbf{X}}\\1
\end{bmatrix}=\begin{bmatrix}
\mathbf{R}^l_{cb}\mathbf{R}^r_{cb}\mathbf{X}^r_{\mathbf{X}}+\mathbf{T}^l_{cb}-\mathbf{R}^l_{cb}{\mathbf{R}^r_{cb}}^\mathrm{T}\mathbf{T}^r_{cb}\\1
\end{bmatrix}\\
\end{aligned}
\end{equation}\label{eqn:3.2}
where $\mathbf{X}^l_\mathbf{X}$ and $\mathbf{X}^r_\mathbf{X}$ are the coordinates of the point $\mathbf{X}$ in the left and right camera coordinates, $\mathbf{H}^l_r$ is the transformation between the right and left camera. The practical implementation of stereo calibration using MATLAB/openCV will be discussed in chapter \ref{Chapter4}.
\section{Stereo Triangulation}\label{sec:st}
The estimation of depth from the stereo image pair is generally called triangulation. It can be formulated using a simple stereo geometry assuming that the stereo image pair is rectified and undistorted. Consider a point in world $\mathbf{p}$ projected onto the left and right image planes as $\mathbf{I_L}_\mathbf{p}$ and $\mathbf{I_R}_\mathbf{p}$. The pixel coordinates of the point $\mathbf{p}$ in the left and right image planes are given by $\left(x^l_{\mathbf{p}},y^l_{\mathbf{p}}\right)$ and $\left(x^r_{\mathbf{p}},y^r_{\mathbf{p}}\right)$. The focal lengths $f_l$ and $f_r$ of the stereo pair are considered equal, $f_l = f_r$ and the coordinates of the principal points along the horizontal i.e., $c^l_x$ and $c^r_x$ are also considered to be equal.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{stereo_geometry.pdf}
\caption{Stereo Geometry in xz-plane}
\label{fig:stereo_geometry}
\end{figure}

Since the image pair is assumed rectified, the y-pixel coordinates of the point $\mathbf{p}$ in the image pair should be equal i.e., $y^l_{\mathbf{p}}=y^r_{\mathbf{p}}$ and the difference which hereinafter referred as \textit{disparity}, $\mathit{d}$ will only be in the x-pixel coordinates of the corresponding points in the image pair $d=x^l_{\mathbf{p}}-x^r_{\mathbf{p}}$. By similar triangles,
\begin{equation}\label{eqn:depth}
\begin{aligned}
&\frac{X_{\mathbf{p}}-B}{Z_{\mathbf{p}}} = \frac{x^l_{\mathbf{p}}}{f}\\
&\frac{X_{\mathbf{p}}}{Z_{\mathbf{p}}} = \frac{x^l_{\mathbf{p}}}{f}\\
&\frac{x^l_{\mathbf{p}}}{f}-\frac{B}{Z_{\mathbf{p}}}=\frac{x^r_{\mathbf{p}}}{f}\\
&Z_{\mathbf{p}} = \frac{fB}{x^l_{\mathbf{p}}-x^r_{\mathbf{p}}}\\
\end{aligned}
\end{equation}
Therefore, the depth of the point $\mathbf{p}$ is inversely proportional to the \textit{disparity} of the point's x-pixel coordinates. Obtaining the coordinates $X_{\mathbf{p}}$ and $Y_{\mathbf{p}}$ is straightforward from the pinhole model.

\section{Stereo Rectification}
The equation \ref{eqn:depth} is derived  assuming the image pair to be rectified. But, in general, the image pair is not rectified i.e., y-pixel coordinates of the corresponding features in the image pair are not equal, $y^l_{\mathbf{p}}\neq y^r_{\mathbf{p}}$. Therefore, the image pair has to be rectified to make them coplanar. The terminologies associated with stereo geometry are defined below \newline
\textbf{Baseline},$B$ is the line separating the camera centres $c_L$ and $c_R$. \newline
\textbf{Epipoles} are the camera center of the second camera projected on the first camera's image plane and vice versa. Or it could be defined as the points of intersection of the baseline with the image planes, $e_L$ and $e_R$.\newline
\textbf{Epipolar plane} contains the baseline and the point in world $\mathbf{p}$. \newline
\textbf{Epipolar line} is the line segment defined by the points of intersection of the epipolar plane with the image plane. i.e.,${I_L}_{\mathbf{p}}e_L$ and${I_R}_{\mathbf{p}}e_R$.

\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{stereo_geometry_2}
\caption{Stereo Geometry in the genuine unrectified case}
\label{fig:stereo_geometry_2}
\end{figure}

The relationship between projections on the left and right image planes of any point in world $\mathbf{p}$ can be expressed using the above terms. The projection on the left image ${I_L}_{\mathbf{p}}$ can have its correspondence anywhere on the epipolar line ${I_R}_{\mathbf{p}}e_R$ on the right image and vice versa. This is called the \textbf{epipolar constraint}. 

Since the pose of the right camera $(\mathbf{R},\mathbf{t})$ has been determined from stereo calibration, the transformation is used to rectify the image pair such that the image pair becomes coplanar. In fact, instead of simply rotating the right camera and aligning with left camera, the rotation matrix $\mathbf{R}$ is split into $\mathbf{R}_l$ and $\mathbf{R}_r$ for the left and right cameras so that both the cameras undergo half the rotation intended by $\mathbf{R}$ and become coplanar with the epipoles at infinity.
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{stereo_geometry_3}
\caption{Stereo Geometry in the rectified case}
\label{fig:stereo_geometry_3}
\end{figure}

But to make the pair row aligned another rotation matrix has to be defined. Considering the principal point of left image as origin, the direction of epipole, $e_1$ is along the horizontal given by the translation vector $\mathbf{t}=\begin{bmatrix}
-t_x&-t_y&-t_z
\end{bmatrix}$ between the two cameras' centres of projection. Therefore,
\begin{equation}
e_1 = \dfrac{\mathbf{t}}{\|\mathbf{t}\|}
\end{equation} 
The second vector,$e_2$ can be constructed from cross product of $e_1$ and the principal ray $[0,0,1]$ such that the normalized vector $e_2$ is given by
\begin{equation}
e_2 = \dfrac{\begin{bmatrix}
-t_y&t_x&0
\end{bmatrix}}{\sqrt{t_y^2+t_x^2}}
\end{equation}
And finally, the third vector is an obvious crossproduct of $e_1$ and $e_2$ i.e., $e_3 = e_1\times e_2$ which together with the vectors $e_1$ and $e_2$ form the rotation matrix which will rotate the left camera about its center of projection.
\begin{equation}
\mathbf{R}_{rectification} = \begin{bmatrix}
e_1\\e_2\\e_3
\end{bmatrix}
\end{equation}
Therefore the comprehensive rotation matrices that will make the image pair both coplanar and collinear are given by
\begin{equation}
\begin{aligned}
\mathbf{R}_l^{\prime} =& \mathbf{R}_{rectification}\mathbf{R}_l\\
\mathbf{R}_r^{\prime} =& \mathbf{R}_{rectification}\mathbf{R}_r
\end{aligned}
\end{equation}

\section{Stereo Matching}
\subsection{Sum of Absolute Differences}
The disparity computation is essentially the most important and difficult part in depth estimation using stereo vision. There are many algorithms to compute stereo disparity \cite{scharstein} but in this work the Sum of Absolute Differences (SAD) is used to compute the disparity. It is a window-based method where the size of the pixel-window and the disparity search range influence the quality of the disparity image. For a pixel ${I_L}_{\mathbf{p}}$ selected in the left image, a window of appropriate size is selected to mask the neighbourhood of ${I_L}_{\mathbf{p}}$. Then, a window of same size scans through the disparity range along the epipolar line in the right image while aggregating a cost $C_{SAD}$ for each pixel increment in the disparity range. The cost, $C_{SAD}$ is given by
\begin{eqnarray}
C_{SAD} = 
min_{d=dmin}^{dmax}\sum\limits_{i=-\frac{m}{2}}^{\frac{m}{2}}\sum\limits_{j=-\frac{m}{2}}^{\frac{m}{2}}|I_L\left({x^l}_{\mathbf{p}}+i,y^l_{\mathbf{p}}+j\right)-I_R\left(x^r_{\mathbf{p}}+i+d,y^r_{\mathbf{p}}+j\right)|
\end{eqnarray}
The pixel in the right image's disparity range that corresponds to the minimum cost, ${I_R}_{\mathbf{p}}$, is considered to be the match for the pixel ${I_L}_{\mathbf{p}}$ in the left image. The difference in the x-pixel values of ${I_L}_{\mathbf{p}}$ and ${I_R}_{\mathbf{p}}$ i.e., $x^l_{\mathbf{p}}-x^r_{\mathbf{p}}$  is the disparity.

\begin{figure}[h!]
\centering
\includegraphics[width=175mm,height = 175mm,keepaspectratio]{thesis_SAD_picture.pdf}
\caption{Illustration of finding the matching pixel in the right image to a pixel of interest in the left image }
\label{fig:thesis_SAD}
\end{figure}

The Sum of Absolute Differences is a simple strategy to compute the disparity by comparing the texture in the stereo pair provided by the window. But as always with any algorithm, SAD does have its shortcomings when computing disparities in texture less areas, foreshortened objects, and occluded regions (occlusions are pattern available in one view and not in the other view). To overcome the problem of computing erroneous disparity values in the above cases there are few constraints incorporated to SAD to eliminate the influence of the affected pixels. Before discussing the constraints, the SAD results obtained on a ground truth dataset will be discussed. The rectified grayscale images are used for the SAD computation and one such ground truth stereo pair famously known as Tsukuba data set \cite{martin} is used for discussion. 
\begin{figure}[h!]

\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=9cm,height=9cm,keepaspectratio]{stereo_left.eps}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
\centering
\includegraphics[width=9cm,height=9cm,keepaspectratio]{stereo_right.eps}
\end{subfigure}
\caption{Tsukuba Stereo pair}
\label{fig:tsukuba_stereo}
\end{figure}
Since the stereo pair is rectified and already under the epipolar constraint, the disparity search is 1D, ie., $d\in \mathbb{R}$ along the horizontal lines based on the calculation of SAD scores in the disparity search range. The quality of the disparity image computed, as mentioned before, will depend on the window-size and the disparity search range. The disparity images for different window-sizes and search ranges are shown below.
\begin{figure}[h!]
\centering
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.8cm,height=5.8cm,keepaspectratio]{disparity_3x3.eps}
\end{subfigure}
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.8cm,height=5.8cm,keepaspectratio]{disparity_5x5.eps}
\end{subfigure}
\caption{Disparity Images for different window sizes and search ranges}
\end{figure}
\begin{figure}[h!]
\centering
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.5cm,height=5.5cm,keepaspectratio]{disparity_9x9.eps}
\end{subfigure}
\begin{subfigure}[t]{0.45\textwidth}
\centering
\includegraphics[width=5.5cm,height=5.5cm,keepaspectratio]{disparity_ground.eps}
\end{subfigure}
\caption{Disparity Image with 9x9 window and Ground truth disparity}
\end{figure}
In the figures, the disparity image with the smallest window has more disparity information especially at depth discontinuities or in other word object boundaries in the intensity image are preserved in the disparity. But it is also noisy with some random depth information i.e., the disparity is not smoothly varying across some regions. On the contrary, the disparity image with the larger window tends to be smoothly varying but it lacks information at depth discontinuities or object boundaries and if the window size is increased further the generated disparity image will altogether lose information on a fine region like the camera handle in the Tsukuba dataset. The disparity search range is also crucial to the quality of the disparity image. If the disparity search range is large particularly in an image where the objects of interest are distant from the camera and thus the horizontal shift between them across the stereo pair is small, there are chances of false correspondences since the disparity computation is based on the texture that is masked by the window in a position and not based on the semantics of the image. Therefore, the disparity search should be conservative based on the considered image to eliminate the possibility of false correspondences.

\subsection{Constraints}

As mentioned in the previous section, there are few constraints which when incorporated in the stereo matching algorithm improve the disparity computation. Only two constraints which are of most importance for this work will be discussed.

\subsubsection{Uniqueness Constraint}
Ideally, every pixel in the left image of the stereo pair should at most have one correspondence in the right image and vice versa. If a pixel has in one of the images more than one correspondence in the other image, then that pixel is deemed invalid for depth computation. There are many reasons for this problem of which matching texture less regions is the most common reason since there is no distinctive feature that can be matched on one to one basis in the stereo pair. Incorporating this constraint will drastically improve the disparity map and in turn the reconstructed 3D point cloud will have reasonable depth values. There are many ways to employ this constraint. 
The formulation used in this work is adapted from MATLAB's implementation where two minimum values of the $C_{SAD}$ vector are considered. The first one is the global minimum $D$ at $d$ from the vector and the second minimum is the global minimum $D^{\prime}$ after excluding the vector indexes $d-1,d,d+1$. Then if \begin{equation}
D^{\prime} < D(1+0.01*\text{uniqueness threshold value})
\end{equation}then, that pixel is labelled invalid. The number of pixels that will be labelled as invalid in a disparity image depends on the uniqueness threshold value selected, higher its value higher the number of invalid pixels. The value has to be carefully chosen depending on the image so as to avoid unnecessary elimination of valid pixels.

\subsubsection{Consistency Check}
The false correspondences also arise from occlusions where a texture is available in one of the images but not the other. Therefore it is very important to identify such regions and label them as occluded. This is implemented by generating a pair of disparity images i.e., one with correspondence search in the right stereo image and the other with the correspondence search in the left stereo image. Now if a pixel is selected in one of the disparity image pair, the disparity value of that pixel should point to the pixel coordinates in the second disparity image which will also have the same disparity value ie., the disparity value of this pixel in the second image will again point to the same pixel in the first image, thus ensuring one to one correspondence, $D_{L_\mathbf{p}}(x_{\mathbf{p}}^l,y_{\mathbf{p}}^l)=d_\mathbf{p}=D_{R_{\mathbf{p}}}(x_{\mathbf{p}}^l-d_{\mathbf{p}},y_{\mathbf{p}}^r)$ where$D_{L_{\mathbf{p}}}$ and $D_{R_{\mathbf{p}}}$ are the disparity images corresponding to the left image coordinates and right image coordinates and $x_{\mathbf{p}}^l-d_{\mathbf{p}}=x_{\mathbf{p}}^r$. If any of the pixels violate this condition then they will be considered invalid and labelled as occlusion. 
\begin{figure}[h!]
\centering
\includegraphics[width=100mm,height = 100mm,keepaspectratio]{consistency_map}
\caption{Disparity Map after consistency check}
\label{fig:consistency_map}
\end{figure}
In figure \ref{fig:consistency_map}, the occluded pixels can be noticed as holes across the image, especially a large occlusion can be seen to the left of the statue and left of the lamp, the regions which are indeed not available in the right camera view and therefore labelled as occlusion.

\section{Depth Estimation}

Once the disparity image of the observed scene is computed, the depth of the scene can be estimated using the equations of section \ref{sec:st} and the calibration parameters estimated. In fact, a unique disparity to depth reprojection matrix is used to calculate the depth values of the pixels of interest in the scene. 
\begin{equation}
\mathbf{Q}\begin{bmatrix}
x_\mathbf{p}^l\\y_\mathbf{p}^l\\d_\mathbf{p}\\1
\end{bmatrix}=\begin{bmatrix}
X_\mathbf{p}\\Y_\mathbf{p}\\Z_\mathbf{p}\\W
\end{bmatrix}
\end{equation}where $\mathbf{Q}=\begin{bmatrix}
1&0&0&-c_x^l\\
0&1&0&-c_y^l\\
0&0&0&f\\
0&0&\dfrac{-1}{t_x}&\dfrac{(c_x^l-c_x^r)}{t_x}
\end{bmatrix}$.
More on the practical implementation of the above discussed theory to real-time data has been provided in chapter \ref{Chapter4}.
